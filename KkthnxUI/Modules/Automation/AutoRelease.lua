local K, C, L = unpack(select(2, ...))
local Module = K:GetModule("Automation")

local _G = _G

local IsInInstance = _G.IsInInstance

-- Auto release the spirit in battlegrounds
function Module.PLAYER_DEAD()
	-- If player has ability to self-resurrect (soulstone, reincarnation, etc), do nothing and quit
	if HasSoulstone() then return end -- HasSoulstone() affects all self-res abilities, returns valid data only while dead

	-- Resurrect if player is in a battleground
	local InstStat, InstType = IsInInstance()
	if InstStat and InstType == "pvp" then
		RepopMe()
		return
	end

	-- Get current location
	SetMapToCurrentZone()
	local areaID = GetCurrentMapAreaID() or 0

	-- Resurrect if player is in Wintergrasp (501)
	if areaID == 501 or areaID == 708 or areaID == 978 or areaID == 1009 or areaID == 1011 then
		RepopMe()
		return
	end

	return
end

function Module:CreateAutoRelease()
	if C["Automation"].AutoRelease ~= true then
		return
	end

	K:RegisterEvent("PLAYER_DEAD", self.PLAYER_DEAD)
end