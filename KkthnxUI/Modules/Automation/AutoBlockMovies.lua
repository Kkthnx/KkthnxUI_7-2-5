local K, C, L = unpack(select(2, ...))
local Module = K:GetModule("Automation")

local _G = _G

local hooksecurefunc = _G.hooksecurefunc
local IsShiftKeyDown = _G.IsShiftKeyDown

function Module:CreateAutoBlockMovies()
	if C["Automation"].BlockMovies == true then
		-- Skip cinematics
		CinematicFrame:HookScript("OnShow", function(self)
			-- Do nothing if shift key is being held
			if IsShiftKeyDown() then return	end
			-- Click the close dialog confirmation button to stop the cinematic
			if self.closeDialog and CinematicFrameCloseDialogConfirmButton then
				CinematicFrameCloseDialogConfirmButton:Click()
				-- Show confirmation message
				K.Print("A cinematic skip was attempted.")
			end
		end)

		-- Skip movies
		hooksecurefunc("MovieFrame_PlayMovie", function(self, movieID)
			-- Do nothing if shift key is being held
			if IsShiftKeyDown() then return	end
			-- Click the close dialog confirmation button to stop the movie
			if self.CloseDialog and self.CloseDialog.ConfirmButton then
				self.CloseDialog.ConfirmButton:Click()
				-- Show confirmation message
				K.Print("A movie skip was attempted.")
				if movieID then
					K.Print("Movie number" .. ": |cffffffff" .. movieID .. "|r")
				end
			end
		end)
	end
end