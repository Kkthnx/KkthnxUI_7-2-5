local K = unpack(select(2, ...))

if not K.CheckAddOnState("Pawn") then return end

local ReskinPawn = CreateFrame("Frame")
ReskinPawn:RegisterEvent("PLAYER_ENTERING_WORLD")
ReskinPawn:RegisterEvent("ADDON_LOADED")
ReskinPawn:SetScript("OnEvent", function(_, event, addon)
	if addon == "Blizzard_InspectUI" or event == "PLAYER_ENTERING_WORLD" and IsAddOnLoaded("Blizzard_InspectUI") then
		K.Delay(0.5, function()
			PawnUI_InspectPawnButton:ClearAllPoints()
			PawnUI_InspectPawnButton:SetPoint("BOTTOMRIGHT", InspectFrameCloseButton, "BOTTOMLEFT", 18, -22)
			PawnUI_InspectPawnButton:SetSize(40, 20)
		end)
	end

    if event == "PLAYER_ENTERING_WORLD" then
        K.Delay(0.5, function()
            PawnUI_InventoryPawnButton:ClearAllPoints()
		    PawnUI_InventoryPawnButton:SetPoint("TOPRIGHT", PaperDollSidebarTab1, "TOPLEFT", -4, 0)
        end)
    end

    if event == "PLAYER_ENTERING_WORLD" then
        ReskinPawn:UnregisterEvent(event)
    end
end)