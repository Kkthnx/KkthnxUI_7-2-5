local K, C = unpack(select(2, ...))
local Module = K:GetModule("Skins")

local _G = _G
local table_insert = _G.table.insert

local function ReskinCompactRaidFrames()
	if not IsAddOnLoaded("Blizzard_CUFProfiles") then return end
	if not IsAddOnLoaded("Blizzard_CompactRaidFrames") then return end
	if not CompactRaidFrameManagerToggleButton then return end

	CompactRaidFrameManagerToggleButton:SetNormalTexture("Interface\\Buttons\\UI-ColorPicker-Buttons")
	CompactRaidFrameManagerToggleButton:GetNormalTexture():SetTexCoord(.15, .39, 0, 1)
	CompactRaidFrameManagerToggleButton:SetSize(15, 15)
	hooksecurefunc("CompactRaidFrameManager_Collapse", function()
		CompactRaidFrameManagerToggleButton:GetNormalTexture():SetTexCoord(.15, .39, 0, 1)
	end)
	hooksecurefunc("CompactRaidFrameManager_Expand", function()
		CompactRaidFrameManagerToggleButton:GetNormalTexture():SetTexCoord(.86, 1, 0, 1)
	end)

	CompactRaidFrameManagerDisplayFrameLeaderOptionsRaidWorldMarkerButton:SetNormalTexture("Interface\\RaidFrame\\Raid-WorldPing")

	for i = 1, 8 do
		select(i, CompactRaidFrameManager:GetRegions()):SetAlpha(0)
	end
	select(1, CompactRaidFrameManagerDisplayFrameFilterOptions:GetRegions()):SetAlpha(0)
	select(1, CompactRaidFrameManagerDisplayFrame:GetRegions()):SetAlpha(0)
	select(4, CompactRaidFrameManagerDisplayFrame:GetRegions()):SetAlpha(0)


	if CompactRaidFrameManager:GetObjectType() == "Texture" then CompactRaidFrameManager = CompactRaidFrameManager:GetParent() end
	local lvl = CompactRaidFrameManager:GetFrameLevel()

	local bg = CreateFrame("Frame", nil, CompactRaidFrameManager)
	bg:SetPoint("TOPLEFT", CompactRaidFrameManager, 4, 0)
	bg:SetPoint("BOTTOMRIGHT", CompactRaidFrameManager, -4, 6)
	bg:SetFrameLevel(lvl == 0 and 1 or lvl - 1)
    bg:CreateBorder()
end
table_insert(Module.NewSkin["KkthnxUI"], ReskinCompactRaidFrames)