local K, C, L = unpack(select(2, ...))

local UnitPopupButtonsExtra = {
	["ARMORY"] = { text = L["Armory Link"] },
	["SEND_WHO"] = { text = PLAYER_MESSAGES },
	["NAME_COPY"] = { text = COPY_NAME },
	["GUILD_ADD"] = { text = L["Guild Invite"] },
	["FRIEND_ADD"] = { text = ADD_FRIEND },
	["QUICK_REPORT"] = { text = REPORT_VERBAL_HARASSMENT },
	["SHOW_PET"] = { text = PET_SHOW_IN_JOURNAL },
}

for k, v in pairs(UnitPopupButtonsExtra) do
	v.dist = 0
	UnitPopupButtons[k] = v
end

tinsert(UnitPopupMenus["FRIEND"], 1, "QUICK_REPORT")
tinsert(UnitPopupMenus["FRIEND"], 1, "ARMORY")
tinsert(UnitPopupMenus["FRIEND"], 1, "NAME_COPY")
tinsert(UnitPopupMenus["FRIEND"], 1, "SEND_WHO")
tinsert(UnitPopupMenus["FRIEND"], 1, "FRIEND_ADD")
tinsert(UnitPopupMenus["FRIEND"], 1, "GUILD_ADD")

tinsert(UnitPopupMenus["CHAT_ROSTER"], 1, "NAME_COPY")
tinsert(UnitPopupMenus["CHAT_ROSTER"], 1, "SEND_WHO")
tinsert(UnitPopupMenus["CHAT_ROSTER"], 1, "FRIEND_ADD")
tinsert(UnitPopupMenus["CHAT_ROSTER"], 1, "INVITE")

tinsert(UnitPopupMenus["GUILD"], 1, "ARMORY")
tinsert(UnitPopupMenus["GUILD"], 1, "NAME_COPY")
tinsert(UnitPopupMenus["GUILD"], 1, "FRIEND_ADD")

local function urlencode(s)
	if not s then return end
	s = string.gsub(s, "([^%w%.%- ])", function(c)
		return format("%%%02X", string.byte(c))
	end)
	return string.gsub(s, " ", "+")
end

local function gethost()
    local host = "https://armory.wow-freakz.com/character/"

	-- if K.Client == "zhCN" then
	-- 	host = "https://www.wowchina.com/zh-cn/character/"
	-- elseif K.Client == "zhTW" then
	-- 	host = "https://worldofwarcraft.com/zh-tw/character/"
	-- else
    -- 	host = ("http://worldofwarcraft.com/en-%s/character/"):format(GetCVar("portal"))

	-- end
	return host
end

local function LFGShowUrl(name)
	if not name then return end
	pName = gsub(name, "%-[^|]+", "")
	pServer = gsub(name, "[^|]+%-", "")
	if pName == pServer then
		pServer = GetRealmName()
	end
	local url = constructUrl(pName,pServer);
	if url then
		local edit_box = ChatEdit_ChooseBoxForSend()
		ChatEdit_ActivateChat(edit_box)
		if url then
			edit_box:Insert(url);
			edit_box:HighlightText();
		end
	end
end

local function popupClick(_, info)
	local editBox
	local name, server = UnitName(info.unit)
	if info.value == "ARMORY" then
		local armory = gethost()..urlencode(server or GetRealmName()).."/"..urlencode(name)
		editBox = ChatEdit_ChooseBoxForSend()
		ChatEdit_ActivateChat(editBox)
		editBox:SetText(armory)
		editBox:HighlightText()
	elseif info.value == "NAME_COPY" then
		if server and server ~= "" then name = name.."-"..server end

		if SendMailNameEditBox and SendMailNameEditBox:IsVisible() then
			SendMailNameEditBox:SetText(name)
			SendMailNameEditBox:HighlightText()
		else
			editBox = ChatEdit_ChooseBoxForSend()
			local hasText = (editBox:GetText() ~= "")
			ChatEdit_ActivateChat(editBox)
			editBox:Insert(name)
			if not hasText then editBox:HighlightText() end
		end
	elseif info.value == "SHOW_PET" then
		if not CollectionsJournal then CollectionsJournal_LoadUI() end
		if not CollectionsJournal:IsShown() then ShowUIPanel(CollectionsJournal) end
		CollectionsJournal_SetTab(CollectionsJournal, 2)
		PetJournal_SelectSpecies(PetJournal, UnitBattlePetSpeciesID(info.unit))
	end
end

hooksecurefunc("UnitPopup_ShowMenu", function(_, _, unit)
	if UIDROPDOWNMENU_MENU_LEVEL > 1 then return end
	if unit and (unit == "target" or string.find(unit, "party") or string.find(unit, "raid")) then
		local info
		if UnitIsPlayer(unit) then
			info = UIDropDownMenu_CreateInfo()
			info.text = UnitPopupButtonsExtra["ARMORY"].text
			info.arg1 = {value = "ARMORY", unit = unit}
			info.func = popupClick
			info.notCheckable = true
			UIDropDownMenu_AddButton(info)
		elseif UnitIsWildBattlePet(unit) or UnitIsBattlePetCompanion(unit) then
			info = UIDropDownMenu_CreateInfo()
			info.text = UnitPopupButtonsExtra["SHOW_PET"].text
			info.arg1 = {value = "SHOW_PET", unit = unit}
			info.func = popupClick
			info.notCheckable = true
			UIDropDownMenu_AddButton(info)
		end

		info = UIDropDownMenu_CreateInfo()
		info.text = UnitPopupButtonsExtra["NAME_COPY"].text
		info.arg1 = {value = "NAME_COPY", unit = unit}
		info.func = popupClick
		info.notCheckable = true
		UIDropDownMenu_AddButton(info)
	end
end)

hooksecurefunc("UnitPopup_OnClick", function(self)
	local unit = UIDROPDOWNMENU_INIT_MENU.unit
	local name = UIDROPDOWNMENU_INIT_MENU.name
	local server = UIDROPDOWNMENU_INIT_MENU.server
	local lineID = UIDROPDOWNMENU_INIT_MENU.lineID
	local fullname = name
	local editBox
	if server and (not unit or UnitRealmRelationship(unit) ~= LE_REALM_RELATION_SAME) then
		fullname = name .. "-" .. server
	end
	if self.value == "ARMORY" then
		local armory = gethost() .. urlencode(server or GetRealmName()) .. "/" .. urlencode(name)
		editBox = ChatEdit_ChooseBoxForSend()
		ChatEdit_ActivateChat(editBox)
		editBox:SetText(armory)
		editBox:HighlightText()
	elseif self.value == "NAME_COPY" then
		if SendMailNameEditBox and SendMailNameEditBox:IsVisible() then
			SendMailNameEditBox:SetText(fullname)
			SendMailNameEditBox:HighlightText()
		else
			editBox = ChatEdit_ChooseBoxForSend()
			local hasText = (editBox:GetText() ~= "")
			ChatEdit_ActivateChat(editBox)
			editBox:Insert(fullname)
			if not hasText then editBox:HighlightText() end
		end
	elseif self.value == "FRIEND_ADD" then
		AddFriend(fullname)
	elseif self.value == "SEND_WHO" then
		SendWho("n-"..name)
	elseif self.value == "GUILD_ADD" then
		GuildInvite(fullname)
	elseif self.value == "QUICK_REPORT" then
		ReportPlayer(PLAYER_REPORT_TYPE_SPAM, lineID)
	end
end)


local function LFGShowUrl(name)
	if not name then return end
	pName = gsub(name, "%-[^|]+", "")
	pServer = gsub(name, "[^|]+%-", "")
	if pName == pServer then
		pServer = GetRealmName()
	end
	local url = constructUrl(pName,pServer);
	if url then
		local edit_box = ChatEdit_ChooseBoxForSend()
		ChatEdit_ActivateChat(edit_box)
		if url then
			edit_box:Insert(url);
			edit_box:HighlightText();
		end
	end
end

------------------------------------------------------------------------
--- PremadeGroup Finder 7.0.0
------------------------------------------------------------------------

local LFG_LIST_SEARCH_ENTRY_MENU = {
    {
        text = nil, --Group name goes here
        isTitle = true,
        notCheckable = true,
    },
    {
        text = WHISPER_LEADER,
        func = function(_, name) ChatFrame_SendTell(name); end,
        notCheckable = true,
        arg1 = nil, --Leader name goes here
        disabled = nil, --Disabled if we don't have a leader name yet or you haven't applied
        tooltipWhileDisabled = 1,
        tooltipOnButton = 1,
        tooltipTitle = nil, --The title to display on mouseover
        tooltipText = nil, --The text to display on mouseover
    },
	{
        text = L["Armory Link"],
		func = function(_, name) LFGShowUrl(name); end,
		notCheckable = true,
		arg1 = nil, --Player name goes here
		disabled = nil, --Disabled if we don't have a name yet
    },
    {
        text = LFG_LIST_REPORT_GROUP_FOR,
        hasArrow = true,
        notCheckable = true,
        menuList = {
            {
                text = LFG_LIST_BAD_NAME,
                func = function(_, id) C_LFGList.ReportSearchResult(id, "lfglistname"); end,
                arg1 = nil, --Search result ID goes here
                notCheckable = true,
            },
            {
                text = LFG_LIST_BAD_DESCRIPTION,
                func = function(_, id) C_LFGList.ReportSearchResult(id, "lfglistcomment"); end,
                arg1 = nil, --Search reuslt ID goes here
                notCheckable = true,
                disabled = nil, --Disabled if the description is just an empty string
            },
            {
                text = LFG_LIST_BAD_VOICE_CHAT_COMMENT,
                func = function(_, id) C_LFGList.ReportSearchResult(id, "lfglistvoicechat"); end,
                arg1 = nil, --Search reuslt ID goes here
                notCheckable = true,
                disabled = nil, --Disabled if the description is just an empty string
            },
            {
                text = LFG_LIST_BAD_LEADER_NAME,
                func = function(_, id) C_LFGList.ReportSearchResult(id, "badplayername"); end,
                arg1 = nil, --Search reuslt ID goes here
                notCheckable = true,
                disabled = nil, --Disabled if we don't have a name for the leader
            },
        },
    },
    {
        text = CANCEL,
        notCheckable = true,
    },
};

function LFGListUtil_GetSearchEntryMenu(resultID)
    local id, activityID, name, comment, voiceChat, iLvl, honorLevel, age, numBNetFriends, numCharFriends, numGuildMates, isDelisted, leaderName, numMembers = C_LFGList.GetSearchResultInfo(resultID);
    local _, appStatus, pendingStatus, appDuration = C_LFGList.GetApplicationInfo(resultID);
    LFG_LIST_SEARCH_ENTRY_MENU[1].text = name;
    LFG_LIST_SEARCH_ENTRY_MENU[2].arg1 = leaderName;
    LFG_LIST_SEARCH_ENTRY_MENU[2].disabled = not leaderName;
    LFG_LIST_SEARCH_ENTRY_MENU[3].arg1 = leaderName;
    LFG_LIST_SEARCH_ENTRY_MENU[3].disabled = not leaderName;
    LFG_LIST_SEARCH_ENTRY_MENU[4].menuList[1].arg1 = resultID;
    LFG_LIST_SEARCH_ENTRY_MENU[4].menuList[2].arg1 = resultID;
    LFG_LIST_SEARCH_ENTRY_MENU[4].menuList[2].disabled = (comment == "");
    LFG_LIST_SEARCH_ENTRY_MENU[4].menuList[3].arg1 = resultID;
    LFG_LIST_SEARCH_ENTRY_MENU[4].menuList[3].disabled = (voiceChat == "");
    LFG_LIST_SEARCH_ENTRY_MENU[4].menuList[4].arg1 = resultID;
    LFG_LIST_SEARCH_ENTRY_MENU[4].menuList[4].disabled = not leaderName;
    return LFG_LIST_SEARCH_ENTRY_MENU;
end

local LFG_LIST_APPLICANT_MEMBER_MENU = {
    {
        text = nil, --Player name goes here
        isTitle = true,
        notCheckable = true,
    },
    {
        text = WHISPER,
        func = function(_, name) ChatFrame_SendTell(name); end,
        notCheckable = true,
        arg1 = nil, --Player name goes here
        disabled = nil, --Disabled if we don't have a name yet
    },
    {
        text = L["Armory Link"],
		func = function(_, name) LFGShowUrl(name); end,
		notCheckable = true,
		arg1 = nil, --Player name goes here
		disabled = nil, --Disabled if we don't have a name yet
    },
    {
        text = LFG_LIST_REPORT_FOR,
        hasArrow = true,
        notCheckable = true,
        menuList = {
            {
                text = LFG_LIST_BAD_PLAYER_NAME,
                notCheckable = true,
                func = function(_, id, memberIdx) C_LFGList.ReportApplicant(id, "badplayername", memberIdx); end,
                arg1 = nil, --Applicant ID goes here
                arg2 = nil, --Applicant Member index goes here
            },
            {
                text = LFG_LIST_BAD_DESCRIPTION,
                notCheckable = true,
                func = function(_, id) C_LFGList.ReportApplicant(id, "lfglistappcomment"); end,
                arg1 = nil, --Applicant ID goes here
            },
        },
    },
    {
        text = IGNORE_PLAYER,
        notCheckable = true,
        func = function(_, name, applicantID) AddIgnore(name); C_LFGList.DeclineApplicant(applicantID); end,
        arg1 = nil, --Player name goes here
        arg2 = nil, --Applicant ID goes here
        disabled = nil, --Disabled if we don't have a name yet
    },
    {
        text = CANCEL,
        notCheckable = true,
    },
};

function LFGListUtil_GetApplicantMemberMenu(applicantID, memberIdx)
    local name, class, localizedClass, level, itemLevel, tank, healer, damage, assignedRole = C_LFGList.GetApplicantMemberInfo(applicantID, memberIdx);
    local id, status, pendingStatus, numMembers, isNew, comment = C_LFGList.GetApplicantInfo(applicantID);
    LFG_LIST_APPLICANT_MEMBER_MENU[1].text = name or " ";
    LFG_LIST_APPLICANT_MEMBER_MENU[2].arg1 = name;
    LFG_LIST_APPLICANT_MEMBER_MENU[2].disabled = not name or (status ~= "applied" and status ~= "invited");
    LFG_LIST_APPLICANT_MEMBER_MENU[3].arg1 = name;
    LFG_LIST_APPLICANT_MEMBER_MENU[3].disabled = not name or (status ~= "applied" and status ~= "invited");
    LFG_LIST_APPLICANT_MEMBER_MENU[4].menuList[1].arg1 = applicantID;
    LFG_LIST_APPLICANT_MEMBER_MENU[4].menuList[1].arg2 = memberIdx;
    LFG_LIST_APPLICANT_MEMBER_MENU[4].menuList[2].arg1 = applicantID;
    LFG_LIST_APPLICANT_MEMBER_MENU[4].menuList[2].disabled = (comment == "");
    LFG_LIST_APPLICANT_MEMBER_MENU[5].arg1 = name;
    LFG_LIST_APPLICANT_MEMBER_MENU[5].arg2 = applicantID;
    LFG_LIST_APPLICANT_MEMBER_MENU[5].disabled = not name;
    return LFG_LIST_APPLICANT_MEMBER_MENU;
end