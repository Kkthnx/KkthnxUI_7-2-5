local K, C = unpack(select(2, ...))
local Module = K:GetModule("Miscellaneous")

local _G = _G
-- local math_floor = math.floor
-- local math_max = math.max
local string_format = string.format

local ATTACK_SPEED = _G.ATTACK_SPEED
local CharacterFrameInsetRight = _G.CharacterFrameInsetRight
local CharacterStatsPane = _G.CharacterStatsPane
local CreateFrame = _G.CreateFrame
local FONT_COLOR_CODE_CLOSE = _G.FONT_COLOR_CODE_CLOSE
local GetMeleeHaste = _G.GetMeleeHaste
local HIGHLIGHT_FONT_COLOR_CODE = _G.HIGHLIGHT_FONT_COLOR_CODE
local IsAddOnLoaded = _G.IsAddOnLoaded
local LE_UNIT_STAT_AGILITY = _G.LE_UNIT_STAT_AGILITY
local LE_UNIT_STAT_INTELLECT = _G.LE_UNIT_STAT_INTELLECT
local LE_UNIT_STAT_STRENGTH = _G.LE_UNIT_STAT_STRENGTH
local PAPERDOLLFRAME_TOOLTIP_FORMAT = _G.PAPERDOLLFRAME_TOOLTIP_FORMAT
local PAPERDOLL_SIDEBARS = _G.PAPERDOLL_SIDEBARS
local PAPERDOLL_STATINFO = _G.PAPERDOLL_STATINFO
local PaperDollFrame_SetLabelAndText = _G.PaperDollFrame_SetLabelAndText
local STAT_ATTACK_SPEED_BASE_TOOLTIP = _G.STAT_ATTACK_SPEED_BASE_TOOLTIP
local UnitAttackSpeed = _G.UnitAttackSpeed
local WEAPON_SPEED = _G.WEAPON_SPEED
local hooksecurefunc = _G.hooksecurefunc

-- Add extra data to the role stats panel while enabling scrolling to prevent stats overflow.
function Module:CreateImprovedStats()
	if C["Misc"].ImprovedStats ~= true then
		return
	end

	if IsAddOnLoaded("DejaCharacterStats") then
		return
	end

	local statPanel = CreateFrame("Frame", nil, CharacterFrameInsetRight)
	statPanel:SetSize(200, 350)
	statPanel:SetPoint("TOP", 0, -5)

	local scrollFrame = CreateFrame("ScrollFrame", nil, statPanel, "UIPanelScrollFrameTemplate")
	scrollFrame:SetAllPoints()
	scrollFrame.ScrollBar:Hide()
	scrollFrame.ScrollBar.Show = K.Noop

	local stat = CreateFrame("Frame", nil, scrollFrame)
	stat:SetSize(200, 1)
	scrollFrame:SetScrollChild(stat)
	CharacterStatsPane:ClearAllPoints()
	CharacterStatsPane:SetParent(stat)
	CharacterStatsPane:SetAllPoints(stat)
	hooksecurefunc("PaperDollFrame_UpdateSidebarTabs", function()
		if (not _G[PAPERDOLL_SIDEBARS[1].frame]:IsShown()) then
			statPanel:Hide()
		else
			statPanel:Show()
		end
	end)

	-- Change default data
	PAPERDOLL_STATCATEGORIES = {
		[1] = {
			categoryFrame = "AttributesCategory",
			stats = {
				[1] = { stat = "STRENGTH", primary = LE_UNIT_STAT_STRENGTH },
				[2] = { stat = "AGILITY", primary = LE_UNIT_STAT_AGILITY },
				[3] = { stat = "INTELLECT", primary = LE_UNIT_STAT_INTELLECT },
				[4] = { stat = "STAMINA" },
				[5] = { stat = "ARMOR" },
				[6] = { stat = "ATTACK_DAMAGE", primary = LE_UNIT_STAT_STRENGTH, roles = { "TANK", "DAMAGER" } },
				[7] = { stat = "ATTACK_AP", hideAt = 0, primary = LE_UNIT_STAT_STRENGTH, roles = { "TANK", "DAMAGER" } },
				[8] = { stat = "ATTACK_ATTACKSPEED", primary = LE_UNIT_STAT_STRENGTH, roles = { "TANK", "DAMAGER" } },
				[9] = { stat = "ATTACK_DAMAGE", primary = LE_UNIT_STAT_AGILITY, roles = { "TANK", "DAMAGER" } },
				[10] = { stat = "ATTACK_AP", hideAt = 0, primary = LE_UNIT_STAT_AGILITY, roles = { "TANK", "DAMAGER" } },
				[11] = { stat = "ATTACK_ATTACKSPEED", primary = LE_UNIT_STAT_AGILITY, roles = { "TANK", "DAMAGER" } },
				[12] = { stat = "SPELLPOWER", hideAt = 0, primary = LE_UNIT_STAT_INTELLECT },
				[13] = { stat = "MANAREGEN", hideAt = 0, primary = LE_UNIT_STAT_INTELLECT },
				[14] = { stat = "ENERGY_REGEN", hideAt = 0, primary = LE_UNIT_STAT_AGILITY },
				[15] = { stat = "RUNE_REGEN", hideAt = 0, primary = LE_UNIT_STAT_STRENGTH },
				[16] = { stat = "FOCUS_REGEN", hideAt = 0, primary = LE_UNIT_STAT_AGILITY },
				[17] = { stat = "MOVESPEED" },
			},
		},
		[2] = {
			categoryFrame = "EnhancementsCategory",
			stats = {
				[1] = { stat = "CRITCHANCE", hideAt = 0 },
				[2] = { stat = "HASTE", hideAt = 0 },
				[3] = { stat = "MASTERY", hideAt = 0 },
				[4] = { stat = "VERSATILITY", hideAt = 0 },
				[5] = { stat = "LIFESTEAL", hideAt = 0 },
				[6] = { stat = "AVOIDANCE", hideAt = 0 },
				[7] = { stat = "DODGE", hideAt = 0, roles = { "TANK" } },
				[8] = { stat = "PARRY", hideAt = 0, roles = { "TANK" } },
				[9] = { stat = "BLOCK", hideAt = 0, roles = { "TANK" } },
			},
		},
	}

	PAPERDOLL_STATINFO["ENERGY_REGEN"].updateFunc = function(statFrame, unit)
		statFrame.numericValue = 0
		PaperDollFrame_SetEnergyRegen(statFrame, unit)
	end

	PAPERDOLL_STATINFO["RUNE_REGEN"].updateFunc = function(statFrame, unit)
		statFrame.numericValue = 0
		PaperDollFrame_SetRuneRegen(statFrame, unit)
	end

	PAPERDOLL_STATINFO["FOCUS_REGEN"].updateFunc = function(statFrame, unit)
		statFrame.numericValue = 0
		PaperDollFrame_SetFocusRegen(statFrame, unit)
	end

	-- Fix Movespeed
	PAPERDOLL_STATINFO["MOVESPEED"].updateFunc = function(statFrame, unit)
		PaperDollFrame_SetMovementSpeed(statFrame, unit)
	end

	function MovementSpeed_OnEnter(statFrame)
		GameTooltip:SetOwner(statFrame, "ANCHOR_RIGHT")
		GameTooltip:SetText(HIGHLIGHT_FONT_COLOR_CODE..string_format(PAPERDOLLFRAME_TOOLTIP_FORMAT, STAT_MOVEMENT_SPEED).." "..string_format("%d%%", statFrame.speed + .5)..FONT_COLOR_CODE_CLOSE)
		GameTooltip:AddLine(string_format(STAT_MOVEMENT_GROUND_TOOLTIP, statFrame.runSpeed + .5))
		if statFrame.unit ~= "pet" then
			GameTooltip:AddLine(string_format(STAT_MOVEMENT_FLIGHT_TOOLTIP, statFrame.flightSpeed + .5))
		end
		GameTooltip:AddLine(string_format(STAT_MOVEMENT_SWIM_TOOLTIP, statFrame.swimSpeed + .5))
		GameTooltip:AddLine(" ")
		GameTooltip:AddLine(string_format(CR_SPEED_TOOLTIP, BreakUpLargeNumbers(GetCombatRating(CR_SPEED)), GetCombatRatingBonus(CR_SPEED)))
		GameTooltip:Show()
	end

	function PaperDollFrame_SetMovementSpeed(statFrame, unit)
		statFrame.wasSwimming = nil
		statFrame.unit = unit
		MovementSpeed_OnUpdate(statFrame)
		statFrame.onEnterFunc = MovementSpeed_OnEnter
		statFrame:Show()
	end

	-- hooksecurefunc("PaperDollFrame_SetItemLevel", function(statFrame, unit)
	-- 	if unit ~= "player" then
	-- 		return
	-- 	end

	-- 	local avgItemLevel, avgItemLevelEquipped = GetAverageItemLevel()
	-- 	local minItemLevel = C_PaperDollInfo_GetMinItemLevel()
	-- 	local displayItemLevel = math_max(minItemLevel or 0, avgItemLevelEquipped)
	-- 	displayItemLevel = string_format("%.1f", displayItemLevel)
	-- 	avgItemLevel = string_format("%.1f", avgItemLevel)

	-- 	if displayItemLevel ~= avgItemLevel then
	-- 		displayItemLevel = displayItemLevel.." / "..avgItemLevel
	-- 	end
	-- 	PaperDollFrame_SetLabelAndText(statFrame, STAT_AVERAGE_ITEM_LEVEL, displayItemLevel, false, displayItemLevel)
	-- end)
end