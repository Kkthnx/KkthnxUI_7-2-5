local K, C = unpack(select(2, ...))
local Module = K:GetModule("Blizzard")

if not Module then
	return
end

local _G = _G
local pairs = _G.pairs
local string_match = _G.string.match
local tonumber = _G.tonumber

local C_Timer_After = _G.C_Timer.After
local CreateFrame = _G.CreateFrame
local GetItemInfo = _G.GetItemInfo
local GetLocale = _G.GetLocale
local GetSpellInfo = _G.GetSpellInfo
local PanelTemplates_GetSelectedTab = _G.PanelTemplates_GetSelectedTab
local blizzardCollectgarbage = _G.collectgarbage
local hooksecurefunc = _G.hooksecurefunc

-- Garbage Collection Is Being Overused And Misused,
-- And It's Causing Lag And Performance Drops.
do
	if C["General"].FixGarbageCollect then
		blizzardCollectgarbage("setpause", 110)
		blizzardCollectgarbage("setstepmul", 200)

		_G.collectgarbage = function(opt, arg)
			if (opt == "collect") or (opt == nil) then
			elseif (opt == "count") then
				return blizzardCollectgarbage(opt, arg)
			elseif (opt == "setpause") then
				return blizzardCollectgarbage("setpause", 110)
			elseif opt == "setstepmul" then
				return blizzardCollectgarbage("setstepmul", 200)
			elseif (opt == "stop") then
			elseif (opt == "restart") then
			elseif (opt == "step") then
				if (arg ~= nil) then
					if (arg <= 10000) then
						return blizzardCollectgarbage(opt, arg)
					end
				else
					return blizzardCollectgarbage(opt, arg)
				end
			else
				return blizzardCollectgarbage(opt, arg)
			end
		end

		-- Memory Usage Is Unrelated To Performance, And Tracking Memory Usage Does Not Track "BAD" Addons.
		-- Developers Can Uncomment This Line To Enable The Functionality When Looking For Memory Leaks,
		-- But For The Average End-user This Is A Completely Pointless Thing To Track.
		_G.UpdateAddOnMemoryUsage = function() end
	end
end

-- Temporary PVP queue taint fix
do
	InterfaceOptionsFrameCancel:SetScript("OnClick", function()
		InterfaceOptionsFrameOkay:Click()
	end)

	if not UIDROPDOWNMENU_VALUE_PATCH_VERSION then
		UIDROPDOWNMENU_VALUE_PATCH_VERSION = 1
		hooksecurefunc("UIDropDownMenu_InitializeHelper", function()
			if UIDROPDOWNMENU_VALUE_PATCH_VERSION ~= 1 then return end
			for i = 1, UIDROPDOWNMENU_MAXLEVELS do
				for j = 1, UIDROPDOWNMENU_MAXBUTTONS do
					local b = _G["DropDownList"..i.."Button"..j]
					while not issecurevariable(b, "value") do
						b.value = nil
						j, b["fx"..j] = j + 1
					end
				end
			end
		end)
	end
end

-- Fix TradeSkill Search
do
	hooksecurefunc("ChatEdit_InsertLink", function(text) -- Shift-Clicked
		-- Change From SearchBox:HasFocus to :IsShown Again
		if text and _G.TradeSkillFrame and _G.TradeSkillFrame:IsShown() then
			local spellId = string_match(text, "enchant:(%d+)")
			local spell = GetSpellInfo(spellId)
			local item = GetItemInfo(string_match(text, "item:(%d+)") or 0)
			local search = spell or item
			if not search then
				return
			end

			-- Search Needs To Be Lowercase For .SetRecipeItemNameFilter
			_G.TradeSkillFrame.SearchBox:SetText(search)

			-- Jump To The Recipe
			if spell then -- Can Only Select Recipes On The Learned Tab
				if PanelTemplates_GetSelectedTab(_G.TradeSkillFrame.RecipeList) == 1 then
					_G.TradeSkillFrame:SelectRecipe(tonumber(spellId))
				end
			elseif item then
				C_Timer_After(.2, function() -- Wait A Bit Or We Cant Select The Recipe Yet
					for _, v in pairs(_G.TradeSkillFrame.RecipeList.dataList) do
						if v.name == item then
							-- TradeSkillFrame.RecipeList:RefreshDisplay() -- Didnt Seem To Help
							_G.TradeSkillFrame:SelectRecipe(v.recipeID)
							return
						end
					end
				end)
			end
		end
	end)
end

do
	hooksecurefunc("ItemAnim_OnLoad", function(self)
		self:UnregisterEvent("ITEM_PUSH")
	end)

	local BlizzardBags = {
		_G.MainMenuBarBackpackButton,
		_G.CharacterBag0Slot,
		_G.CharacterBag1Slot,
		_G.CharacterBag2Slot,
		_G.CharacterBag3Slot,

		_G.StuffingFBag0Slot,
		_G.StuffingFBag1Slot,
		_G.StuffingFBag2Slot,
		_G.StuffingFBag3Slot,
	}

	for _, Button in pairs(BlizzardBags) do
		Button:UnregisterEvent("ITEM_PUSH") -- Gets Rid Of The Animation
	end
end

function Module:CreateBlizzBugFixes()
	CreateFrame("Frame"):SetScript("OnUpdate", function()
		if _G.LFRBrowseFrame.timeToClear then
			_G.LFRBrowseFrame.timeToClear = nil
		end
	end)
end