local K, C, L = unpack(select(2, ...))
local Module = K:GetModule("WorldMap")

if not Module then
	return
end

-- Sourced: Leatrix_Maps by Leatrix

-- Function to refresh overlays (Blizzard_SharedMapDataProviders\MapExplorationDataProvider)
-- Initialise counters
local WorldMapDetailFrame, WorldMapTitleButton, WorldMapFrame = _G.WorldMapDetailFrame, _G.WorldMapTitleButton, _G.WorldMapFrame
local createdtex, texcount = 0, 0
-- Create local texture table
local MapTex = {}
function Module:CreateMapReveal()
	local function RefMap()
		-- Hide textures from previous map
		if texcount > 0 then
			for i = 1, texcount do
				MapTex[i]:Hide()
			end
			texcount = 0
		end

		-- Get current map
		local filename, _, _, _, sub = GetMapInfo()
		if sub then return end
		if not filename then return end

		local texpath = format([[Interface\WorldMap\%s\]], filename)
		local zone = K.WorldMapPlusData[filename] or {}

		-- Create new textures for current map
		for _, num in next, zone do
			local tname, texwidth, texheight, offsetx, offsety = strsplit(":", num)
			local texturename = texpath..tname
			local numtexwide, numtextall = math.ceil(texwidth / 256), math.ceil(texheight / 256)

			-- Work out how many textures are needed to fill the map
			local neededtex = texcount + numtextall * numtexwide

			-- Create the textures
			if neededtex > createdtex then
				for j = createdtex + 1, neededtex do
					MapTex[j] = WorldMapDetailFrame:CreateTexture(nil, "ARTWORK")
				end
				createdtex = neededtex
			end

			-- Process textures
			for j = 1, numtextall do
				local texturepxheight, texturefileheight
				if j < numtextall then
					texturepxheight = 256
					texturefileheight = 256
				else
					texturepxheight = texheight % 256
					if texturepxheight == 0 then
						texturepxheight = 256
					end
					texturefileheight = 16
					while texturefileheight < texturepxheight do
						texturefileheight = texturefileheight * 2
					end
				end

				for k = 1, numtexwide do
					if texcount > createdtex then return end
					texcount = texcount + 1
					local texture = MapTex[texcount]
					local texturepxwidth
					local texturefilewidth
					if k < numtexwide then
						texturepxwidth = 256
						texturefilewidth = 256
					else
						texturepxwidth = texwidth % 256
						if texturepxwidth == 0 then
							texturepxwidth = 256
						end
						texturefilewidth = 16
						while texturefilewidth < texturepxwidth do
							texturefilewidth = texturefilewidth * 2
						end
					end
					texture:SetWidth(texturepxwidth)
					texture:SetHeight(texturepxheight)
					texture:SetTexCoord(0, texturepxwidth / texturefilewidth, 0, texturepxheight / texturefileheight)
					texture:ClearAllPoints()
					texture:SetPoint("TOPLEFT", WorldMapDetailFrame, "TOPLEFT", offsetx + (256 * (k - 1)), -(offsety + (256 * (j - 1))))
					texture:SetTexture(texturename..(((j - 1) * numtexwide) + k))
					texture:Show()
				end
			end
		end
	end

	-- Create checkbox
	local frame = CreateFrame("CheckButton", "FogCheckBox", WorldMapFrame.BorderFrame, "OptionsCheckButtonTemplate")
	frame:SetPoint("TOPRIGHT", WorldMapFrame.BorderFrame, -254, 0)
	frame:SetSize(24, 24)
	frame.text = K.CreateFontString(frame, 12, "NONE", "Reveal", "system", "LEFT", 25, 0)


	-- Handle clicks
	frame:SetScript("OnClick", function()
		if frame:GetChecked() == true then
			KkthnxUIData[GetRealmName()][UnitName("player")].RevealWorldMap = true
			if WorldMapFrame:IsShown() then RefMap() end
		else
			KkthnxUIData[GetRealmName()][UnitName("player")].RevealWorldMap = false
			if texcount > 0 then
				for i = 1, texcount do MapTex[i]:Hide()	end
				texcount = 0
			end
		end
	end)

	function frame.UpdateTooltip(self)
		if (GameTooltip:IsForbidden()) then
			return
		end

		GameTooltip:SetOwner(self, "ANCHOR_TOP", 0, 10)

		local r, g, b = 0.2, 1.0, 0.2

		if KkthnxUIData[GetRealmName()][UnitName("player")].RevealWorldMap == true then
			GameTooltip:AddLine(L["Hide Undiscovered Areas"])
			GameTooltip:AddLine(" ")
			GameTooltip:AddLine(L["Disable to hide areas."], r, g, b)
		else
			GameTooltip:AddLine(L["Reveal Hidden Areas"])
			GameTooltip:AddLine(" ")
			GameTooltip:AddLine(L["Enable to show hidden areas."], r, g, b)
		end

		GameTooltip:Show()
	end

	frame:HookScript("OnEnter", function(self)
		if (GameTooltip:IsForbidden()) then
			return
		end

		self:UpdateTooltip()
	end)

	frame:HookScript("OnLeave", function()
		if (GameTooltip:IsForbidden()) then
			return
		end

		GameTooltip:Hide()
	end)

	-- Set checkbox state
	frame:SetScript("OnShow", function()
		frame:SetChecked(KkthnxUIData[GetRealmName()][UnitName("player")].RevealWorldMap)
	end)

	-- Update map
	hooksecurefunc("WorldMapFrame_Update", function()
		if WorldMapFrame:IsShown() and KkthnxUIData[GetRealmName()][UnitName("player")].RevealWorldMap then
			RefMap()
		end
	end)
end

function Module:CreateMapLinks()
	-- Get localised Wowhead URL
	local wowheadLoc
	local GameLocale = GetLocale()
	if GameLocale == "deDE" then wowheadLoc = "de.wowhead.com"
	elseif GameLocale == "esMX" then wowheadLoc = "es.wowhead.com"
	elseif GameLocale == "esES" then wowheadLoc = "es.wowhead.com"
	elseif GameLocale == "frFR" then wowheadLoc = "fr.wowhead.com"
	elseif GameLocale == "itIT" then wowheadLoc = "it.wowhead.com"
	elseif GameLocale == "ptBR" then wowheadLoc = "pt.wowhead.com"
	elseif GameLocale == "ruRU" then wowheadLoc = "ru.wowhead.com"
	elseif GameLocale == "koKR" then wowheadLoc = "ko.wowhead.com"
	elseif GameLocale == "zhCN" then wowheadLoc = "cn.wowhead.com"
	elseif GameLocale == "zhTW" then wowheadLoc = "cn.wowhead.com"
	else							 wowheadLoc = "wowhead.com"
	end

	----------------------------------------------------------------------
	-- Achievements frame
	----------------------------------------------------------------------

	-- Achievement link function
	local function DoWowheadAchievementFunc()

		-- Create editbox
		local aEB = CreateFrame("EditBox", nil, AchievementFrame)
		aEB:ClearAllPoints()
		aEB:SetPoint("BOTTOMRIGHT", -50, 1)
		aEB:SetHeight(16)
		aEB:SetFontObject("GameFontNormalSmall")
		aEB:SetBlinkSpeed(0)
		aEB:SetJustifyH("RIGHT")
		aEB:SetAutoFocus(false)
		aEB:EnableKeyboard(false)
		aEB:SetHitRectInsets(90, 0, 0, 0)
		aEB:SetScript("OnKeyDown", function() end)
		aEB:SetScript("OnMouseUp", function()
			if aEB:IsMouseOver() then
				aEB:HighlightText()
			else
				aEB:HighlightText(0, 0)
			end
		end)

		-- Create hidden font string (used for setting width of editbox)
		aEB.z = aEB:CreateFontString(nil, 'ARTWORK', 'GameFontNormalSmall')
		aEB.z:Hide()

		-- Store last link in case editbox is cleared
		local lastAchievementLink

		-- Function to set editbox value
		hooksecurefunc("AchievementFrameAchievements_SelectButton", function(self)
			local achievementID = self.id or nil
			if achievementID then
				-- Set editbox text
				aEB:SetText("https://" .. wowheadLoc .. "/achievement=" .. achievementID)
				lastAchievementLink = aEB:GetText()
				-- Set hidden fontstring then resize editbox to match
				aEB.z:SetText(aEB:GetText())
				aEB:SetWidth(aEB.z:GetStringWidth() + 90)
				-- Get achievement title for tooltip
				local achievementLink = GetAchievementLink(self.id)
				if achievementLink then
					aEB.tiptext = achievementLink:match("%[(.-)%]") .. "|n" .. "Press CTRL/C to copy."
				end
				-- Show the editbox
				aEB:Show()
			end
		end)

		-- Create tooltip
		aEB:HookScript("OnEnter", function()
			aEB:HighlightText()
			aEB:SetFocus()
			GameTooltip:SetOwner(aEB, "ANCHOR_TOP", 0, 10)
			GameTooltip:SetText(aEB.tiptext, nil, nil, nil, nil, true)
			GameTooltip:Show()
		end)

		aEB:HookScript("OnLeave", function()
			-- Set link text again if it's changed since it was set
			if aEB:GetText() ~= lastAchievementLink then aEB:SetText(lastAchievementLink) end
			aEB:HighlightText(0, 0)
			aEB:ClearFocus()
			GameTooltip:Hide()
		end)

		-- Hide editbox when achievement is deselected
		hooksecurefunc("AchievementFrameAchievements_ClearSelection", function(self) aEB:Hide()	end)
		hooksecurefunc("AchievementCategoryButton_OnClick", function(self) aEB:Hide() end)

	end

	-- Run function when achievement UI is loaded
	if IsAddOnLoaded("Blizzard_AchievementUI") then
		DoWowheadAchievementFunc()
	else
		local waitAchievementsFrame = CreateFrame("FRAME")
		waitAchievementsFrame:RegisterEvent("ADDON_LOADED")
		waitAchievementsFrame:SetScript("OnEvent", function(self, event, arg1)
			if arg1 == "Blizzard_AchievementUI" then
				DoWowheadAchievementFunc()
				waitAchievementsFrame:UnregisterAllEvents()
			end
		end)
	end

	----------------------------------------------------------------------
	-- Encounter Journal frame
	----------------------------------------------------------------------

	local function DoEJFunc()

		-- Hide the title bar
		EncounterJournalTitleText:Hide()

		-- Create editbox
		local eEB = CreateFrame("EditBox", nil, EncounterJournal)
		eEB:ClearAllPoints()
		eEB:SetPoint("TOPLEFT", 70, -4)
		eEB:SetHeight(16)
		eEB:SetFontObject("GameFontNormal")
		eEB:SetBlinkSpeed(0)
		eEB:SetAutoFocus(false)
		eEB:EnableKeyboard(false)
		eEB:SetHitRectInsets(0, 90, 0, 0)
		eEB:SetScript("OnKeyDown", function() end)
		eEB:SetScript("OnMouseUp", function()
			if eEB:IsMouseOver() then
				eEB:HighlightText()
			else
				eEB:HighlightText(0, 0)
			end
		end)

		-- Create hidden font string (used for setting width of editbox)
		eEB.z = eEB:CreateFontString(nil, 'ARTWORK', 'GameFontNormal')
		eEB.z:Hide()

		-- Store last link in case user clears editbox
		local lastEJLink

		-- Function to set editbox value
		hooksecurefunc("EncounterJournal_DisplayInstance", function()
			local void, void, void, void, void, void, dungeonAreaMapID, link = EJ_GetInstanceInfo()
			local mapID, areaID = GetAreaMapInfo(dungeonAreaMapID)
			if areaID then
				-- Set editbox text
				eEB:SetText("https://" .. wowheadLoc .. "/zone=" .. areaID)
				lastEJLink = eEB:GetText()
				-- Set hidden fontstring then resize editbox to match
				eEB.z:SetText(eEB:GetText())
				eEB:SetWidth(eEB.z:GetStringWidth() + 90)
				-- Get achievement title for tooltip
				if link then
					eEB.tiptext = link:match("%[(.-)%]") .. "|n" .. "Press CTRL/C to copy."
				end
				-- Show the editbox
				eEB:Show()
			end
		end)

		-- Create tooltip
		eEB:HookScript("OnEnter", function()
			eEB:HighlightText()
			eEB:SetFocus()
			GameTooltip:SetOwner(eEB, "ANCHOR_BOTTOM", 0, -10)
			GameTooltip:SetText(eEB.tiptext, nil, nil, nil, nil, true)
			GameTooltip:Show()
		end)

		eEB:HookScript("OnLeave", function()
			-- Set link text again if it's changed since it was set
			if eEB:GetText() ~= lastEJLink then eEB:SetText(lastEJLink) end
			eEB:HighlightText(0, 0)
			eEB:ClearFocus()
			GameTooltip:Hide()
		end)

		-- Hide editbox when instance list is shown
		hooksecurefunc("EncounterJournal_ListInstances", function()
			eEB:Hide()
		end)

	end

	-- Run function when encounter journal is loaded
	if IsAddOnLoaded("Blizzard_EncounterJournal") then
		DoEJFunc()
	else
		local waitJournalFrame = CreateFrame("FRAME")
		waitJournalFrame:RegisterEvent("ADDON_LOADED")
		waitJournalFrame:SetScript("OnEvent", function(self, event, arg1)
			if arg1 == "Blizzard_EncounterJournal" then
				DoEJFunc()
				waitJournalFrame:UnregisterAllEvents()
			end
		end)
	end

	----------------------------------------------------------------------
	-- World map frame
	----------------------------------------------------------------------

	-- Hide the title text
	WorldMapFrameTitleText:Hide()

	-- Create editbox
	local mEB = CreateFrame("EditBox", nil, WorldMapFrame.BorderFrame)
	mEB:ClearAllPoints()
	mEB:SetPoint("TOPLEFT", 100, -4)
	mEB:SetHeight(16)
	mEB:SetFontObject("GameFontNormal")
	mEB:SetBlinkSpeed(0)
	mEB:SetAutoFocus(false)
	mEB:EnableKeyboard(false)
	mEB:SetHitRectInsets(0, 90, 0, 0)
	mEB:SetScript("OnKeyDown", function() end)
	mEB:SetScript("OnMouseUp", function()
		if mEB:IsMouseOver() then
			mEB:HighlightText()
		else
			mEB:HighlightText(0, 0)
		end
	end)

	-- Create hidden font string (used for setting width of editbox)
	mEB.z = mEB:CreateFontString(nil, 'ARTWORK', 'GameFontNormal')
	mEB.z:Hide()

	-- Function to set editbox value
	local function SetQuestInBox()
		local questID
		if QuestMapFrame.DetailsFrame:IsShown() then
			-- Get quest ID from currently showing quest in details panel
			questID = QuestMapFrame_GetDetailQuestID()
		else
			-- Get quest ID from currently selected quest on world map
			questID = GetSuperTrackedQuestID()
		end
		if questID then
			-- Hide editbox if quest ID is invalid
			if questID == 0 then mEB:Hide() else mEB:Show() end
			-- Set editbox text
			mEB:SetText("https://" .. wowheadLoc .. "/quest=" .. questID)
			-- Set hidden fontstring then resize editbox to match
			mEB.z:SetText(mEB:GetText())
			mEB:SetWidth(mEB.z:GetStringWidth() + 90)
			-- Get quest title for tooltip
			local questLink = GetQuestLink(questID) or nil
			if questLink then
				mEB.tiptext = questLink:match("%[(.-)%]") .. "|n" .. "Press CTRL/C to copy."
			else
				mEB.tiptext = ""
				if mEB:IsMouseOver() and WorldMapTooltip:IsShown() then WorldMapTooltip:Hide() end
			end
		end
	end

	-- Set URL when super tracked quest changes and on startup
	mEB:RegisterEvent("SUPER_TRACKED_QUEST_CHANGED")
	mEB:SetScript("OnEvent", SetQuestInBox)
	SetQuestInBox()

	-- Set URL when quest details frame is shown or hidden
	hooksecurefunc("QuestMapFrame_ShowQuestDetails", SetQuestInBox)
	hooksecurefunc("QuestMapFrame_CloseQuestDetails", SetQuestInBox)

	-- Create tooltip
	mEB:HookScript("OnEnter", function()
		mEB:HighlightText()
		mEB:SetFocus()
		WorldMapTooltip:SetOwner(mEB, "ANCHOR_BOTTOM", 0, -10)
		WorldMapTooltip:SetText(mEB.tiptext, nil, nil, nil, nil, true)
		WorldMapTooltip:Show()
	end)

	mEB:HookScript("OnLeave", function()
		mEB:HighlightText(0, 0)
		mEB:ClearFocus()
		WorldMapTooltip:Hide()
		SetQuestInBox()
	end)
end

function Module:CreateMapIcons()
	local dnTex = "Interface\\Minimap\\Dungeon"
	local rdTex = "Interface\\Minimap\\Raid"
	local ptTex = "Interface\\Minimap\\Vehicle-AllianceMagePortal"

	local pTable = {
		-- Portals: Suramar
		{category = "ShowSuramarPortals"	, name = "Astravar Harbor"				, continent = 1220	, zoneID = 1033		, x = 875		, y = 3634	, texture = ptTex		, desc = "Portal", reqQuest = 44740, remQuest = nil},
		{category = "ShowSuramarPortals"	, name = "Evermoon Terrace"				, continent = 1220	, zoneID = 1033		, x = 530		, y = 3771	, texture = ptTex		, desc = "Portal", reqQuest = 42889, remQuest = nil},
		{category = "ShowSuramarPortals"	, name = "Falanaar"						, continent = 1220	, zoneID = 1033		, x = 2369		, y = 5442	, texture = ptTex		, desc = "Portal", reqQuest = 42230, remQuest = nil},
		{category = "ShowSuramarPortals"	, name = "Felsoul Hold"					, continent = 1220	, zoneID = 1033		, x = 623		, y = 4477	, texture = ptTex		, desc = "Portal", reqQuest = 41575, remQuest = nil},
		{category = "ShowSuramarPortals"	, name = "Lunastre Estate"				, continent = 1220	, zoneID = 1033		, x = 511		, y = 4233	, texture = ptTex		, desc = "Portal", reqQuest = 43811, remQuest = nil},
		{category = "ShowSuramarPortals"	, name = "Moon Guard Stronghold"			, continent = 1220	, zoneID = 1033		, x = 3037		, y = 4947	, texture = ptTex		, desc = "Portal", reqQuest = 43808, remQuest = nil},
		{category = "ShowSuramarPortals"	, name = "Ruins of Elune'eth"			, continent = 1220	, zoneID = 1033		, x = 1697		, y = 4653	, texture = ptTex		, desc = "Portal", reqQuest = 40956, remQuest = nil},
		{category = "ShowSuramarPortals"	, name = "Sanctum of Order"				, continent = 1220	, zoneID = 1033		, x = 1203		, y = 4248	, texture = ptTex		, desc = "Portal", reqQuest = 43813, remQuest = nil},
		{category = "ShowSuramarPortals"	, name = "Tel'anor"						, continent = 1220	, zoneID = 1033		, x = 2141		, y = 4324	, texture = ptTex		, desc = "Portal", reqQuest = 43809, remQuest = nil},
		{category = "ShowSuramarPortals"	, name = "The Waning Crescent"			, continent = 1220	, zoneID = 1033		, x = 433		, y = 4007	, texture = ptTex		, desc = "Portal", reqQuest = 42487, remQuest = 38649},
		{category = "ShowSuramarPortals"	, name = "Twilight Vineyards"			, continent = 1220	, zoneID = 1033		, x = 1209		, y = 3105	, texture = ptTex		, desc = "Portal", reqQuest = 44084, remQuest = nil},

		-- Portals: Stormshield (Ashran)
		{category = "ShowSuramarPortals"	, name = "Ironforge"						, continent = 1116	, zoneID = 1009		, x = 3675		, y = -3979	, texture = ptTex		, desc = "Portal"},
		{category = "ShowSuramarPortals"	, name = "Darnassus"						, continent = 1116	, zoneID = 1009		, x = 3614		, y = -4062	, texture = ptTex		, desc = "Portal"},
		{category = "ShowSuramarPortals"	, name = "Stormwind"						, continent = 1116	, zoneID = 1009		, x = 3735		, y = -4043	, texture = ptTex		, desc = "Portal"},
		{category = "ShowSuramarPortals"	, name = "Lion's Watch"					, continent = 1116	, zoneID = 1009		, x = 3720		, y = -3875	, texture = ptTex		, desc = "Portal", reqQuest = 38445},

		-- Portals: Warspear (Ashran)
		{category = "ShowSuramarPortals"	, name = "Orgrimmar"						, continent = 1116	, zoneID = 1011		, x = 5272		, y = -4053	, texture = ptTex		, desc = "Portal"},
		{category = "ShowSuramarPortals"	, name = "Undercity"						, continent = 1116	, zoneID = 1011		, x = 5413		, y = -4096	, texture = ptTex		, desc = "Portal"},
		{category = "ShowSuramarPortals"	, name = "Thunder Bluff"					, continent = 1116	, zoneID = 1011		, x = 5412		, y = -3989	, texture = ptTex		, desc = "Portal"},
		{category = "ShowSuramarPortals"	, name = "Vol'mar"						, continent = 1116	, zoneID = 1011		, x = 5308		, y = -4015	, texture = ptTex		, desc = "Portal", reqQuest = 37935},

		-- Dungeons: Eastern Kingdoms
		{category = "ShowDungeonLocs"		, name = "Baradin Hold"					, linkID = 752		, continent = 732	, zoneID = 708		, x = -1204		, y = 1079	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Blackrock Caverns"				, linkID = 753		, continent = 0		, zoneID = 28		, x = -7615		, y = -1242	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Blackrock Caverns"				, linkID = 753		, continent = 0		, zoneID = 29		, x = -7615		, y = -1242	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Blackrock Caverns"				, linkID = 753		, continent = 0		, zoneID = 28		, x = -7570		, y = -1328	, texture = dnTex		, desc = "Dungeon", level = 15},
		{category = "ShowDungeonLocs"		, name = "Blackrock Caverns"				, linkID = 753		, continent = 0		, zoneID = 29		, x = -7570		, y = -1328	, texture = dnTex		, desc = "Dungeon", level = 15},
		{category = "ShowDungeonLocs"		, name = "Blackrock Depths"				, linkID = 704		, continent = 0		, zoneID = 28		, x = -7179		, y = -922	, texture = dnTex		, desc = "Dungeon", level = 16},
		{category = "ShowDungeonLocs"		, name = "Blackrock Depths"				, linkID = 704		, continent = 0		, zoneID = 29		, x = -7179		, y = -922	, texture = dnTex		, desc = "Dungeon", level = 16},
		{category = "ShowDungeonLocs"		, name = "Blackrock Mountain"			, linkID = nil		, continent = 0		, zoneID = 29		, x = -7781		, y = -1128	, texture = rdTex		, desc = "Blackrock Caverns" .. ", " .. "Blackrock Spire" .. ",|n" .. "Blackrock Depths" .. ", " .. "Blackwing Lair" .. ",|n" .. "Molten Core"},
		{category = "ShowDungeonLocs"		, name = "Blackrock Mountain"			, linkID = nil		, continent = 0		, zoneID = 28		, x = -7365		, y = -1101	, texture = rdTex		, desc = "Blackrock Caverns" .. ", " .. "Blackrock Spire" .. ",|n" .. "Blackrock Depths" .. ", " .. "Blackwing Lair" .. ",|n" .. "Molten Core"},
		{category = "ShowDungeonLocs"		, name = "Blackrock Spire"				, linkID = nil		, continent = 0		, zoneID = 28		, x = -7524		, y = -1230	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Blackrock Spire"				, linkID = nil		, continent = 0		, zoneID = 29		, x = -7524		, y = -1230	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Blackwing Descent"				, linkID = 754		, continent = 0		, zoneID = 29		, x = -7538		, y = -1196	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Blackwing Lair"				, linkID = 755		, continent = 0		, zoneID = 28		, x = -7662		, y = -1218	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Blackwing Lair"				, linkID = 755		, continent = 0		, zoneID = 29		, x = -7662		, y = -1218	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Gnomeregan"					, linkID = 691		, continent = 0		, zoneID = 27		, x = -5184		, y = 603	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Gnomeregan"					, linkID = 691		, continent = 0		, zoneID = 895		, x = -5183		, y = 598	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Gnomeregan"					, linkID = 691		, continent = 0		, zoneID = 27		, x = -5145		, y = 898	, texture = dnTex		, desc = "Dungeon", level = 10},
		{category = "ShowDungeonLocs"		, name = "Grim Batol"					, linkID = 757		, continent = 0		, zoneID = 700		, x = -4058		, y = -3450	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Karazhan"						, linkID = 799		, continent = 0		, zoneID = 32		, x = -11111	, y = -2006	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Lower Blackrock Spire"			, linkID = 721		, continent = 0		, zoneID = 28		, x = -7518		, y = -1334	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Lower Blackrock Spire"			, linkID = 721		, continent = 0		, zoneID = 29		, x = -7518		, y = -1334	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Magisters' Terrace"			, linkID = 798		, continent = 530	, zoneID = 499		, x = 12884		, y = -7333	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Molten Core"					, linkID = 696		, continent = 0		, zoneID = 28		, x = -7509		, y = -1040	, texture = rdTex		, desc = "Raid", level = 14},
		{category = "ShowDungeonLocs"		, name = "Molten Core"					, linkID = 696		, continent = 0		, zoneID = 29		, x = -7509		, y = -1040	, texture = rdTex		, desc = "Raid", level = 14},
		{category = "ShowDungeonLocs"		, name = "Molten Core"					, linkID = 696		, continent = 0		, zoneID = 28		, x = -7509		, y = -1040	, texture = rdTex		, desc = "Raid", level = 16},
		{category = "ShowDungeonLocs"		, name = "Molten Core"					, linkID = 696		, continent = 0		, zoneID = 29		, x = -7509		, y = -1040	, texture = rdTex		, desc = "Raid", level = 16},
		{category = "ShowDungeonLocs"		, name = "Return to Karazhan"			, linkID = 1115		, continent = 0		, zoneID = 32		, x = -11037	, y = -2001	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Scarlet Halls"					, linkID = 871		, continent = 0		, zoneID = 20		, x = 2867		, y = -822	, texture = dnTex		, desc = "Dungeon", level = 13},
		{category = "ShowDungeonLocs"		, name = "Scarlet Monastery"				, linkID = nil		, continent = 0		, zoneID = 20		, x = 2828		, y = -698	, texture = dnTex		, desc = "Scarlet Monastery" .. ", " .. "Scarlet Halls"},
		{category = "ShowDungeonLocs"		, name = "Scarlet Monastery"				, linkID = 874		, continent = 0		, zoneID = 20		, x = 2916		, y = -802	, texture = dnTex		, desc = "Dungeon", level = 13},
		{category = "ShowDungeonLocs"		, name = "Scholomance"					, linkID = 898		, continent = 0		, zoneID = 22		, x = 1262		, y = -2581	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Shadowfang Keep"				, linkID = 764		, continent = 0		, zoneID = 21		, x = -233		, y = 1564	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Stratholme: Crusader's Square"	, linkID = 765		, continent = 0		, zoneID = 23		, x = 3391		, y = -3407	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Stratholme: The Gauntlet"		, linkID = 765.2	, continent = 0		, zoneID = 23		, x = 3183		, y = -4038	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Sunwell Plateau"				, linkID = 789		, continent = 530	, zoneID = 499		, x = 12559		, y = -6774	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Temple of Atal'Hakkar"			, linkID = 687		, continent = 0		, zoneID = 38		, x = -10429	, y = -3829	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "The Bastion of Twilight"		, linkID = 758		, continent = 0		, zoneID = 700		, x = -4895		, y = -4230	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "The Deadmines"					, linkID = 756		, continent = 0		, zoneID = 39		, x = -11075	, y = 1527	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "The Deadmines"					, linkID = 756		, continent = 0		, zoneID = 39		, x = -11207	, y = 1674	, texture = dnTex		, desc = "Dungeon", level = 17},
		{category = "ShowDungeonLocs"		, name = "The Stockade"					, linkID = 690		, continent = 0		, zoneID = 301		, x = -8806		, y = 813	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "The Stockade"					, linkID = 690		, continent = 0		, zoneID = 30		, x = -8806		, y = 813	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Throne of the Tides"			, linkID = 767		, continent = 0		, zoneID = 613		, x = -5720		, y = 5345	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Throne of the Tides"			, linkID = 767		, continent = 0		, zoneID = 614		, x = -5720		, y = 5345	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Uldaman"						, linkID = 692		, continent = 0		, zoneID = 17		, x = -6093		, y = -3183	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Uldaman"						, linkID = 692		, continent = 0		, zoneID = 17		, x = -6065		, y = -2955	, texture = dnTex		, desc = "Dungeon", level = 18},
		{category = "ShowDungeonLocs"		, name = "Upper Blackrock Spire"			, linkID = 995		, continent = 0		, zoneID = 28		, x = -7487		, y = -1324	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Upper Blackrock Spire"			, linkID = 995		, continent = 0		, zoneID = 29		, x = -7487		, y = -1324	, texture = dnTex		, desc = "Dungeon", level = 14},
		{category = "ShowDungeonLocs"		, name = "Zul'Aman"						, linkID = 781		, continent = 530	, zoneID = 463		, x = 6851		, y = -7991	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Zul'Gurub"						, linkID = 793		, continent = 0		, zoneID = 37		, x = -11915	, y = -1207	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Zul'Gurub"						, linkID = 793		, continent = 0		, zoneID = 689		, x = -11915	, y = -1207	, texture = dnTex		, desc = "Dungeon"},

		-- Dungeons: Kalimdor
		{category = "ShowDungeonLocs"		, name = "Blackfathom Deeps"				, linkID = 688		, continent = 1		, zoneID = 43		, x = 4137		, y = 887	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Caverns of Time"				, linkID = nil		, continent = 1		, zoneID = 161		, x = -8172		, y = -4746	, texture = rdTex		, desc = "Black Morass" .. ", " .. "Culling of Stratholme" .. ",|n" .. "Dragon Soul" .. ", " .. "End Time" .. ", " .. "Hour of Twilight" .. ",|n" .. "Hyjal Summit" .. ", " .. "Old Hillsbrad Foothills" .. ",|n" .. "Well of Eternity"},
		{category = "ShowDungeonLocs"		, name = "Dire Maul: Capital Gardens"	, linkID = 699.2	, continent = 1		, zoneID = 121		, x = -3767		, y = 1249	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Dire Maul: Gordok Commons"		, linkID = 699		, continent = 1		, zoneID = 121		, x = -3520		, y = 1101	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Dire Maul: Warpwood Quarter"	, linkID = 699.5	, continent = 1		, zoneID = 121		, x = -3769		, y = 934	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Firelands"						, linkID = 800		, continent = 1		, zoneID = 606		, x = 3988		, y = -2944	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Halls of Origination"			, linkID = 759		, continent = 1		, zoneID = 720		, x = -10182	, y = -1996	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Lost City of the Tol'vir"		, linkID = 747		, continent = 1		, zoneID = 720		, x = -10681	, y = -1307	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Maraudon"						, linkID = 750		, continent = 1		, zoneID = 101		, x = -1422		, y = 2922	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Maraudon: Earth Song Falls"	, linkID = 750.2	, continent = 1		, zoneID = 101		, x = -1379		, y = 2915	, texture = dnTex		, desc = "Dungeon", level = 22},
		{category = "ShowDungeonLocs"		, name = "Maraudon: Foulspore Cavern"	, linkID = 750		, continent = 1		, zoneID = 101		, x = -1473		, y = 2617	, texture = dnTex		, desc = "Dungeon", level = 21},
		{category = "ShowDungeonLocs"		, name = "Maraudon: The Wicked Grotto"	, linkID = 750		, continent = 1		, zoneID = 101		, x = -1182		, y = 2876	, texture = dnTex		, desc = "Dungeon", level = 22},
		{category = "ShowDungeonLocs"		, name = "Onyxia's Lair"					, linkID = 718		, continent = 1		, zoneID = 141		, x = -4718		, y = -3734	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Ragefire Chasm"				, linkID = 680		, continent = 1		, zoneID = 4		, x = 1816		, y = -4418	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Ragefire Chasm"				, linkID = 680		, continent = 1		, zoneID = 321		, x = 1815		, y = -4418	, texture = dnTex		, desc = "Dungeon", level = 2},
		{category = "ShowDungeonLocs"		, name = "Razorfen Downs"				, linkID = 760		, continent = 1		, zoneID = 61		, x = -4722		, y = -2342	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Razorfen Kraul"				, linkID = 761		, continent = 1		, zoneID = 607		, x = -4465		, y = -1666	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Ruins of Ahn'Qiraj"			, linkID = 717		, continent = 1		, zoneID = 261		, x = -8414		, y = 1504	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Ruins of Ahn'Qiraj"			, linkID = 717		, continent = 1		, zoneID = 772		, x = -8414		, y = 1504	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Temple of Ahn'Qiraj"			, linkID = 766		, continent = 1		, zoneID = 261		, x = -8236		, y = 1993	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Temple of Ahn'Qiraj"			, linkID = 766		, continent = 1		, zoneID = 772		, x = -8236		, y = 1993	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "The Vortex Pinnacle"			, linkID = 769		, continent = 1		, zoneID = 720		, x = -11514	, y = -2311	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Throne of the Four Winds"		, linkID = 773		, continent = 1		, zoneID = 720		, x = -11354	, y = 59	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Wailing Caverns"				, linkID = 749		, continent = 1		, zoneID = 11		, x = -837		, y = -2033	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Wailing Caverns"				, linkID = 749		, continent = 1		, zoneID = 11		, x = -742		, y = -2216	, texture = dnTex		, desc = "Dungeon", level = 20},
		{category = "ShowDungeonLocs"		, name = "Zul'Farrak"					, linkID = 686		, continent = 1		, zoneID = 161		, x = -6798		, y = -2891	, texture = dnTex		, desc = "Dungeon"},

		-- Dungeons: Caverns of Time
		{category = "ShowDungeonLocs"		, name = "Dragon Soul"					, linkID = 824		, continent = 1		, zoneID = 161		, x = -8267		, y = -4514	, texture = rdTex		, desc = "Raid", level = 18},
		{category = "ShowDungeonLocs"		, name = "End Time"						, linkID = 820		, continent = 1		, zoneID = 161		, x = -8293		, y = -4458	, texture = dnTex		, desc = "Dungeon", level = 18},
		{category = "ShowDungeonLocs"		, name = "Hour of Twilight"				, linkID = 819		, continent = 1		, zoneID = 161		, x = -8292		, y = -4584	, texture = dnTex		, desc = "Dungeon", level = 18},
		{category = "ShowDungeonLocs"		, name = "Hyjal Summit"					, linkID = 775		, continent = 1		, zoneID = 161		, x = -8171		, y = -4168	, texture = rdTex		, desc = "Raid", level = 18},
		{category = "ShowDungeonLocs"		, name = "Old Hillsbrad Foothills"		, linkID = 734		, continent = 1		, zoneID = 161		, x = -8348		, y = -4060	, texture = dnTex		, desc = "Dungeon", level = 18},
		{category = "ShowDungeonLocs"		, name = "The Black Morass"				, linkID = 733		, continent = 1		, zoneID = 161		, x = -8752		, y = -4194	, texture = dnTex		, desc = "Dungeon", level = 18},
		{category = "ShowDungeonLocs"		, name = "The Culling of Stratholme"		, linkID = 521		, continent = 1		, zoneID = 161		, x = -8755		, y = -4454	, texture = dnTex		, desc = "Dungeon", level = 18},
		{category = "ShowDungeonLocs"		, name = "Well of Eternity"				, linkID = 816		, continent = 1		, zoneID = 161		, x = -8595		, y = -4004	, texture = dnTex		, desc = "Dungeon", level = 18},

		-- Dungeons: Outland
		{category = "ShowDungeonLocs"		, name = "Auchenai Crypts"				, linkID = 722		, continent = 530	, zoneID = 478		, x = -3361		, y = 5136	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Black Temple"					, linkID = 796		, continent = 530	, zoneID = 473		, x = -3648		, y = 318	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Coilfang Reservoir"			, linkID = nil		, continent = 530	, zoneID = 467		, x = 562		, y = 6942	, texture = rdTex		, desc = "Serpentshrine Cavern" .. ", " .. "Slave Pens" .. ",|n" .. "Steamvault" .. ", " .. "Underbog"},
		{category = "ShowDungeonLocs"		, name = "Gruul's Lair"					, linkID = 776		, continent = 530	, zoneID = 475		, x = 3530		, y = 5121	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Hellfire Ramparts"				, linkID = 797		, continent = 530	, zoneID = 465		, x = -363		, y = 3078	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Magtheridon's Lair"			, linkID = 779		, continent = 530	, zoneID = 465		, x = -339		, y = 3132	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Mana-Tombs"					, linkID = 732		, continent = 530	, zoneID = 478		, x = -3168		, y = 4943	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Sethekk Halls"					, linkID = 723		, continent = 530	, zoneID = 478		, x = -3362		, y = 4750	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Shadow Labyrinth"				, linkID = 724		, continent = 530	, zoneID = 478		, x = -3554		, y = 4943	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "The Arcatraz"					, linkID = 731		, continent = 530	, zoneID = 479		, x = 3309		, y = 1337	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "The Blood Furnace"				, linkID = 725		, continent = 530	, zoneID = 465		, x = -301		, y = 3160	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "The Botanica"					, linkID = 729		, continent = 530	, zoneID = 479		, x = 3409		, y = 1486	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "The Eye"						, linkID = 782		, continent = 530	, zoneID = 479		, x = 3087		, y = 1379	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "The Mechanar"					, linkID = 730		, continent = 530	, zoneID = 479		, x = 2865		, y = 1549	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "The Shattered Halls"			, linkID = 710		, continent = 530	, zoneID = 465		, x = -309		, y = 3076	, texture = dnTex		, desc = "Dungeon"},

		-- Dungeons: Northrend
		{category = "ShowDungeonLocs"		, name = "Azjol-Nerub"					, linkID = nil		, continent = 571	, zoneID = 488		, x = 3727		, y = 2152	, texture = dnTex		, desc = "Azjol-Nerub" .. ", " .. "The Old Kingdom"},
		{category = "ShowDungeonLocs"		, name = "Drak'Tharon Keep"				, linkID = 534		, continent = 571	, zoneID = 490		, x = 4574		, y = -2029	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Drak'Tharon Keep"				, linkID = 534		, continent = 571	, zoneID = 496		, x = 4882		, y = -2046	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Gundrak"						, linkID = 530		, continent = 571	, zoneID = 496		, x = 6965		, y = -4407	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Gundrak (rear entrance)"		, linkID = 530		, continent = 571	, zoneID = 496		, x = 6709		, y = -4654	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Halls of Lightning"			, linkID = 525		, continent = 571	, zoneID = 495		, x = 9176		, y = -1377	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Halls of Stone"				, linkID = 526		, continent = 571	, zoneID = 495		, x = 8922		, y = -979	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Icecrown Citadel"				, linkID = 604		, continent = 571	, zoneID = 492		, x = 5855		, y = 2103	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Naxxramas"						, linkID = 535		, continent = 571	, zoneID = 488		, x = 3668		, y = -1260	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "The Frozen Halls"				, linkID = nil		, continent = 571	, zoneID = 492		, x = 5691		, y = 2143	, texture = dnTex		, desc = "The Forge of Souls" .. ", " .. "The Pit of Saron" .. ",|n" .. "The Halls of Reflection"},
		{category = "ShowDungeonLocs"		, name = "The Nexus"						, linkID = nil		, continent = 571	, zoneID = 486		, x = 3864		, y = 6986	, texture = rdTex		, desc = "The Nexus" .. ", " .. "The Oculus" .. ",|n" .. "The Eye of Eternity"},
		{category = "ShowDungeonLocs"		, name = "The Violet Hold"				, linkID = 536		, continent = 571	, zoneID = 504		, x = 5690		, y = 500	, texture = dnTex		, desc = "Dungeon", level = 1},
		{category = "ShowDungeonLocs"		, name = "Trial of the Champion"			, linkID = 542		, continent = 571	, zoneID = 492		, x = 8571		, y = 792	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Trial of the Crusader"			, linkID = 543		, continent = 571	, zoneID = 492		, x = 8515		, y = 733	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Ulduar"						, linkID = 529		, continent = 571	, zoneID = 495		, x = 9348		, y = -1114	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Utgarde Keep"					, linkID = 523		, continent = 571	, zoneID = 491		, x = 1101		, y = -4903	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Utgarde Pinnacle"				, linkID = 524		, continent = 571	, zoneID = 491		, x = 1240		, y = -4857	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Vault of Archavon"				, linkID = 532		, continent = 571	, zoneID = 501		, x = 5377		, y = 2840	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Wyrmrest Temple"				, linkID = nil		, continent = 571	, zoneID = 488		, x = 3672		, y = 289	, texture = rdTex		, desc = "The Ruby Sanctum" .. ", " .. "The Obsidian Sanctum"},

		-- Dungeons: Deepholm
		{category = "ShowDungeonLocs"		, name = "The Stonecore"					, linkID = 768		, continent = 646	, zoneID = 640		, x = 1024		, y = 637	, texture = dnTex		, desc = "Dungeon"},

		-- Dungeons: Pandaria
		{category = "ShowDungeonLocs"		, name = "Gate of the Setting Sun"		, linkID = 875		, continent = 870	, zoneID = 811		, x = 694		, y = 2080	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Heart of Fear"					, linkID = 897		, continent = 870	, zoneID = 858		, x = 166		, y = 4060	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Mogu'shan Palace"				, linkID = 885		, continent = 870	, zoneID = 811		, x = 1392		, y = 436	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Mogu'shan Vaults"				, linkID = 896		, continent = 870	, zoneID = 809		, x = 3982		, y = 1109	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Shado-Pan Monastery"			, linkID = 877		, continent = 870	, zoneID = 809		, x = 3636		, y = 2536	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Siege of Niuzao Temple"		, linkID = 887		, continent = 870	, zoneID = 810		, x = 1435		, y = 5086	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Siege of Orgrimmar"			, linkID = 953		, continent = 870	, zoneID = 811		, x = 1203		, y = 644	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Stormstout Brewery"			, linkID = 876		, continent = 870	, zoneID = 807		, x = -712		, y = 1263	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Temple of the Jade Serpent"	, linkID = 867		, continent = 870	, zoneID = 806		, x = 960		, y = -2468	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Terrace of Endless Spring"		, linkID = 886		, continent = 870	, zoneID = 873		, x = 955		, y = -56	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Throne of Thunder"				, linkID = 930		, continent = 1064	, zoneID = 928		, x = 7253		, y = 5026	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Throne of Thunder"				, linkID = 930		, continent = 870	, zoneID = 810		, x = 1926		, y = 4222	, texture = rdTex		, desc = "Portal", reqQuest = 32681}, -- Alliance
		{category = "ShowDungeonLocs"		, name = "Throne of Thunder"				, linkID = 930		, continent = 870	, zoneID = 810		, x = 1926		, y = 4222	, texture = rdTex		, desc = "Portal", reqQuest = 32680}, -- Horde

		-- Throne of Thunder
		{category = "ShowDungeonLocs"		, name = "Eternal Guardian"				, linkID = nil		, continent = 1098	, zoneID = 930		, x = 6288		, y = 4922	, texture = "Interface\\MINIMAP\\TempleofKotmogu_ball_orange", level = 3},
		{category = "ShowDungeonLocs"		, name = "Eternal Guardian"				, linkID = nil		, continent = 1098	, zoneID = 930		, x = 6110		, y = 4670	, texture = "Interface\\MINIMAP\\TempleofKotmogu_ball_orange", level = 3},
		{category = "ShowDungeonLocs"		, name = "Eternal Guardian"				, linkID = nil		, continent = 1098	, zoneID = 930		, x = 6495		, y = 4741	, texture = "Interface\\MINIMAP\\TempleofKotmogu_ball_orange", level = 3},

		-- Dungeons: Draenor
		{category = "ShowDungeonLocs"		, name = "Auchindoun"					, linkID = 984		, continent = 1116	, zoneID = 946		, x = 1489		, y = 3079	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Blackrock Foundry"				, linkID = 988		, continent = 1116	, zoneID = 949		, x = 8029		, y = 870	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Bloodmaul Slag Mines"			, linkID = 964		, continent = 1116	, zoneID = 941		, x = 7266		, y = 4458	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Grimrail Depot"				, linkID = 993		, continent = 1116	, zoneID = 949		, x = 7840		, y = 551	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Hellfire Citadel"				, linkID = 1026		, continent = 1116	, zoneID = 945		, x = 4058		, y = -683	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Highmaul"						, linkID = 994		, continent = 1116	, zoneID = 950		, x = 3471		, y = 7434	, texture = rdTex		, desc = "Raid"},
		{category = "ShowDungeonLocs"		, name = "Iron Docks"					, linkID = 987		, continent = 1116	, zoneID = 949		, x = 8854		, y = 1359	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Shadowmoon Burial Grounds"		, linkID = 969		, continent = 1116	, zoneID = 947		, x = 766		, y = 128	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "Skyreach"						, linkID = 989		, continent = 1116	, zoneID = 948		, x = 27		, y = 2525	, texture = dnTex		, desc = "Dungeon"},
		{category = "ShowDungeonLocs"		, name = "The Everbloom"					, linkID = 1008		, continent = 1116	, zoneID = 949		, x = 7110		, y = 197	, texture = dnTex		, desc = "Dungeon"},

		-- Dungeons: Legion
		{category = "ShowDungeonLocs"		, name = "Violet Hold"					, linkID = 1066		, continent = 1220	, zoneID = 1014		, x = -959		, y = 4328	, texture = dnTex		, desc = "Dungeon", level = 10},
	}

	local btnTable = {}

	-- POI is shown if the following conditions are met:
	-- 1. If reqQuest is present, player must have completed quest with that ID
	-- 2. If remQuest is present, player must not have completed quest with that ID
	-- 3. If level is present, map must be showing that level
	-- 4. If level is not present, map must be showing level 0

	-- Map IDs
	-- https://wow.gamepedia.com/MapID#Eastern_Kingdoms

	local function mapPOIfunc()
		if WorldMapFrame:IsShown() then
			local worldMapID = GetCurrentMapAreaID()
			local mapID = GetAreaMapInfo(worldMapID)
			for k, v in pairs(pTable) do
				if worldMapID == v.zoneID and mapID == v.continent then
					-- Create POI button if required
					if not btnTable[k] then
						local button = CreateFrame("Button", nil, WorldMapPOIFrame)
						btnTable[k] = button
						button:SetSize(20, 20)
						button.Texture = button:CreateTexture(btnTable[k.."Texture"], "BACKGROUND")
						button.Texture:ClearAllPoints()
						button.Texture:SetPoint("CENTER", button)
						button.Texture:SetTexture(v.texture)
						button.Texture:SetTexCoord(0, 1, 0, 1)
						button.Texture:SetSize(30, 30)
						if v.linkID then button:SetHighlightTexture(v.texture) end
						button.HighlightTexture = button:CreateTexture(btnTable[k .. "HighlightTexture"], "HIGHLIGHT")
						button:SetScript("OnEnter", WorldMapPOI_OnEnter)
						button:SetScript("OnLeave", WorldMapPOI_OnLeave)
						button:SetScript("OnClick", function()
							if v.linkID then
								local id, level = strsplit(".", v.linkID)
								SetMapByID(id)
								if level then SetDungeonMapLevel(level) end
							end
						end)
						button.name = v.name
						button.poiID = 0
						button.description = v.desc
					end
					-- Calculate screen coordinates from world coordinates
					local void, locLeft, locTop, locRight, locBottom = GetCurrentMapZone()
					if locLeft and locTop and locRight and locBottom then
						-- Show or hide POI button
						local currLevel, minY, minX, maxY, maxX = GetCurrentMapDungeonLevel()
						if (not v.reqQuest or IsQuestFlaggedCompleted(v.reqQuest)) and (not v.remQuest or not IsQuestFlaggedCompleted(v.remQuest)) and (not v.level and currLevel == 0 or v.level and v.level == currLevel) then
							if minY and minX and maxY and maxX then
								WorldMapPOIFrame_AnchorPOI(btnTable[k], (maxY - v.y) / abs(maxY - minY), (maxX - v.x) / abs(maxX - minX), 201)
							else
								WorldMapPOIFrame_AnchorPOI(btnTable[k], (locLeft - v.y) / abs(locRight - locLeft), (locTop - v.x) / abs(locBottom - locTop), 201)
							end
							btnTable[k]:Show()
						else
							btnTable[k]:Hide()
						end
					end
				else
					-- Wrong zone, hide POI button
					if btnTable[k] and btnTable[k]:IsShown() then
						btnTable[k]:Hide()
					end
				end
			end
		end
	end

	hooksecurefunc("WorldMapFrame_Update", mapPOIfunc)
end

function Module:CreateWorldMapPlus()
	if C["WorldMap"].WorldMapPlus ~= true then
		return
	end

	self:CreateMapReveal()
	self:CreateMapIcons()
	self:CreateMapLinks()
end