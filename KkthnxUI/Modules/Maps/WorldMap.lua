local K, C = unpack(select(2, ...))
local module = K:NewModule("WorldMap")

function module:SetLargeWorldMap()
	if InCombatLockdown() then return end

	WorldMapFrame:SetParent(UIParent)
	WorldMapFrame:EnableKeyboard(false)
	WorldMapFrame:SetScale(1)
	WorldMapFrame:EnableMouse(true)
	WorldMapTooltip:SetFrameStrata("TOOLTIP")
	WorldMapCompareTooltip1:SetFrameStrata("TOOLTIP")
	WorldMapCompareTooltip2:SetFrameStrata("TOOLTIP")

	if WorldMapFrame:GetAttribute("UIPanelLayout-area") ~= "center" then
		SetUIPanelAttribute(WorldMapFrame, "area", "center");
	end

	if WorldMapFrame:GetAttribute("UIPanelLayout-allowOtherPanels") ~= true then
		SetUIPanelAttribute(WorldMapFrame, "allowOtherPanels", true)
	end

	WorldMapFrame:ClearAllPoints()
	WorldMapFrame:SetPoint("CENTER", UIParent, "CENTER", 0, 100)
	WorldMapFrame:SetSize(1002, 668)
end

function module:SetSmallWorldMap()
	if InCombatLockdown() then return end
end

function module:PLAYER_REGEN_ENABLED()

end

function module:PLAYER_REGEN_DISABLED()

end

function module:WorldMap_OnUpdate(elapsed)
	module.Interval = module.Interval - elapsed

	if module.Interval < 0 then
		local InInstance, _ = IsInInstance()

		local X, Y = GetPlayerMapPosition("player")

		if not GetPlayerMapPosition("player") then
			X = 0
			Y = 0
		end

		X = math.floor(100 * X)
		Y = math.floor(100 * Y)

		if X ~= 0 and Y ~= 0 then
			module.Coords.PlayerText:SetText(K.MyClassColor..PLAYER..":|r "..X..", "..Y)
		else
			module.Coords.PlayerText:SetText(" ")
		end


		local Scale = WorldMapDetailFrame:GetEffectiveScale()
		local Width = WorldMapDetailFrame:GetWidth()
		local Height = WorldMapDetailFrame:GetHeight()
		local CenterX, CenterY = WorldMapDetailFrame:GetCenter()

		X, Y = GetCursorPosition()

		local AdjustedX = (X / Scale - (CenterX - (Width / 2))) / Width
		local AdjustedY = (CenterY + (Height / 2 ) - Y / Scale) / Height

		if (AdjustedX >= 0 and AdjustedY >= 0 and AdjustedX <= 1 and AdjustedY <= 1) then
			AdjustedX = math.floor(100 * AdjustedX)
			AdjustedY = math.floor(100 * AdjustedY)

			module.Coords.MouseText:SetText(K.MyClassColor..MOUSE_LABEL..":|r "..AdjustedX..", "..AdjustedY)
		else
			module.Coords.MouseText:SetText(" ")
		end

		module.Interval = module.UpdateEveryXSeconds
	end
end

function module:CreateCoords()
	self.Coords = CreateFrame("Frame", nil, WorldMapFrame)
	self.Coords:SetFrameLevel(90)

	self.Coords.PlayerText = K.CreateFontString(self.Coords, 14, "OUTLINE", "", false)
	self.Coords.MouseText = K.CreateFontString(self.Coords, 14, "OUTLINE", "", false)
	self.Coords.PlayerText:SetTextColor(1, 1, 0)
	self.Coords.MouseText:SetTextColor(1, 1, 0)
	self.Coords.PlayerText:SetPoint("BOTTOMLEFT", WorldMapFrame.BorderFrame, "BOTTOMLEFT", 5, 5)
	self.Coords.PlayerText:SetText(K.MyClassColor.."Player:|r 0, 0")
	self.Coords.MouseText:SetPoint("LEFT", self.Coords.PlayerText, "RIGHT", 5, 0)
	self.Coords.MouseText:SetText(K.MyClassColor.."Mouse:|r 0, 0")
end

function module:OnEnable()
	-- Generate Coords
	if C["WorldMap"].Coordinates then
		module.Interval = 0.3 -- I do not see a point in updating this any faster.
		module.UpdateEveryXSeconds = module.Interval
		module:CreateCoords()
		WorldMapFrame:HookScript("OnUpdate", module.WorldMap_OnUpdate)
	end

	if (C["WorldMap"].SmallWorldMap) then
		BlackoutWorld:SetTexture(nil)
		hooksecurefunc("WorldMap_ToggleSizeDown", module.SetSmallWorldMap)
		hooksecurefunc("WorldMap_ToggleSizeUp", module.SetLargeWorldMap)

		K:RegisterEvent("PLAYER_REGEN_ENABLED", self.PLAYER_REGEN_ENABLED)
		K:RegisterEvent("PLAYER_REGEN_DISABLED", self.PLAYER_REGEN_DISABLED)

		if WORLDMAP_SETTINGS.size == WORLDMAP_FULLMAP_SIZE then
			self:SetLargeWorldMap()
		elseif WORLDMAP_SETTINGS.size == WORLDMAP_WINDOWED_SIZE then
			self:SetSmallWorldMap()
		end
	end

	--Set alpha used when moving
	WORLD_MAP_MIN_ALPHA = C["WorldMap"].AlphaWhenMoving
	if not InCombatLockdown() then
		SetCVar("mapAnimMinAlpha", C["WorldMap"].AlphaWhenMoving)
		--Enable/Disable map fading when moving
		SetCVar("mapFade", (C["WorldMap"].FadeWhenMoving == true and 1 or 0))
	end

	self:CreateWorldMapPlus()
end