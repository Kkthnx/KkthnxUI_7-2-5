local K, C = unpack(select(2, ...))
if C["DataText"].System ~= true then
	return
end

local module = K:GetModule("Infobar")
local info = module:RegisterInfobar("KkthnxUILocation", {"TOP", Minimap, "TOP", 0, -4})

local zoneInfo = {
	sanctuary = {SANCTUARY_TERRITORY, {0.035, 0.58, 0.84}},
	arena = {FREE_FOR_ALL_TERRITORY, {0.84, 0.03, 0.03}},
	friendly = {FACTION_CONTROLLED_TERRITORY, {0.05, 0.85, 0.03}},
	hostile = {FACTION_CONTROLLED_TERRITORY, {0.84, 0.03, 0.03}},
	contested = {CONTESTED_TERRITORY, {0.9, 0.85, 0.05}},
	combat = {COMBAT_ZONE, {0.84, 0.03, 0.03}},
	neutral = {format(FACTION_CONTROLLED_TERRITORY, FACTION_STANDING_LABEL4), {0.9, 0.85, 0.05}}
}

local subzone, zone, pvp
local coordX, coordY = 0, 0

local function formatCoords()
	return format("%.1f, %.1f", coordX*100, coordY*100)
end

info.eventList = {
	"ZONE_CHANGED",
	"ZONE_CHANGED_INDOORS",
	"ZONE_CHANGED_NEW_AREA",
	"PLAYER_ENTERING_WORLD",
}

info.onEvent = function(self)
	subzone, zone, pvp = GetSubZoneText(), GetZoneText(), {GetZonePVPInfo()}
	if not pvp[1] then pvp[1] = "neutral" end
	local r, g, b = unpack(zoneInfo[pvp[1]][2])
	self.text:SetText((subzone ~= "") and subzone or zone)
	self.text:SetTextColor(r, g, b)
end

local function isInvasionPoint()
	local mapName = GetMapInfo()
	local invaName = C_Scenario.GetInfo()
	if mapName and mapName:match("InvasionPoint") and invaName then
		return true
	end
end

info.onEnter = function(self)
	GameTooltip:SetOwner(self, "ANCHOR_BOTTOM", 0, -15)
	GameTooltip:ClearLines()

	if GetPlayerMapPosition("player") then
		self:SetScript("OnUpdate", function(self, elapsed)
			self.timer = (self.timer or 0) + elapsed
			if self.timer > .1 then
				coordX, coordY = GetPlayerMapPosition("player")
				self:GetScript("OnEnter")(self)
				self.timer = 0
			end
		end)
	end
	GameTooltip:AddLine(format("%s |cffffffff(%s)", zone, formatCoords()))

	if pvp[1] and not IsInInstance() then
		local r, g, b = unpack(zoneInfo[pvp[1]][2])
		if subzone and subzone ~= zone then
			GameTooltip:AddLine(" ")
			GameTooltip:AddLine(subzone, r, g, b)
		end
		GameTooltip:AddLine(format(zoneInfo[pvp[1]][1], pvp[3] or ""), r, g, b)
	end

	GameTooltip:AddLine(" ")
	GameTooltip:AddLine( "|TInterface\\TUTORIALFRAME\\UI-TUTORIAL-FRAME:13:11:0:-1:512:512:12:66:230:307|t ".."WorldMap".." ", 1, 1, 1, 1, 1, 1)
	if isInvasionPoint() then
		GameTooltip:AddLine( "|TInterface\\TUTORIALFRAME\\UI-TUTORIAL-FRAME:13:11:0:-1:512:512:12:66:333:411|t ".."Search Invasion Group".." ", 1, 1, 1, 1, 1, 1)
	end
	GameTooltip:AddLine( "|TInterface\\TUTORIALFRAME\\UI-TUTORIAL-FRAME:13:11:0:-1:512:512:12:66:333:411|t ".."Send My Pos".." ", 1, 1, 1, 1, 1, 1)
	GameTooltip:Show()
end

info.onLeave = function()
	info:SetScript("OnUpdate", nil)
	GameTooltip:Hide()
end

info.onMouseUp = function(_, btn)
	if btn == "LeftButton" then
		if InCombatLockdown() then UIErrorsFrame:AddMessage(K.InfoColor..ERR_NOT_IN_COMBAT) return end
		ToggleFrame(WorldMapFrame)
	elseif btn == "RightButton" then
		local hasUnit = UnitExists("target") and not UnitIsPlayer("target")
		local unitName = nil
		if hasUnit then unitName = UnitName("target") end
		ChatFrame_OpenChat(format("%s: %s (%s) %s", "My Position", zone, formatCoords(), unitName or ""), SELECTED_DOCK_FRAME)
	end
end