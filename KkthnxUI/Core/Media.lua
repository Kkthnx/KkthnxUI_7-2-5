local K, C = unpack(select(2, ...))

local _G = _G

local CreateFont = _G.CreateFont
local CreateFrame = _G.CreateFrame

local KkthnxUIMedia = CreateFrame("Frame", "KkthnxUIFonts")

-- Create our own fonts
local KkthnxUIFont = CreateFont("KkthnxUIFont")
KkthnxUIFont:SetFont(C["Media"].Font, 12)
KkthnxUIFont:SetShadowColor(0, 0, 0, 1)
KkthnxUIFont:SetShadowOffset(1.25, -1.25)

local KkthnxUIFontOutline = CreateFont("KkthnxUIFontOutline")
KkthnxUIFontOutline:SetFont(C["Media"].Font, 12, "OUTLINE")
KkthnxUIFontOutline:SetShadowColor(0, 0, 0, 0)
KkthnxUIFontOutline:SetShadowOffset(0, -0)

local PTSansNarrowFont = CreateFont("SansNarrowFont")
PTSansNarrowFont:SetFont([[Interface\AddOns\KkthnxUI\Media\Fonts\PT_Sans_Narrow.ttf]], 12)
PTSansNarrowFont:SetShadowColor(0, 0, 0, 1)
PTSansNarrowFont:SetShadowOffset(1.25, -1.25)

local PTSansNarrowFontOutline = CreateFont("SansNarrowFontOutline")
PTSansNarrowFontOutline:SetFont([[Interface\AddOns\KkthnxUI\Media\Fonts\PT_Sans_Narrow.ttf]], 12, "OUTLINE")
PTSansNarrowFontOutline:SetShadowColor(0, 0, 0, 0)
PTSansNarrowFontOutline:SetShadowOffset(0, -0)

local ExpresswayFont = CreateFont("ExpresswayFont")
ExpresswayFont:SetFont([[Interface\AddOns\KkthnxUI\Media\Fonts\Expressway.ttf]], 12)
ExpresswayFont:SetShadowColor(0, 0, 0, 1)
ExpresswayFont:SetShadowOffset(1.25, -1.25)

local ExpresswayFontOutline = CreateFont("ExpresswayFontOutline")
ExpresswayFontOutline:SetFont([[Interface\AddOns\KkthnxUI\Media\Fonts\Expressway.ttf]], 12, "OUTLINE")
ExpresswayFontOutline:SetShadowColor(0, 0, 0, 0)
ExpresswayFontOutline:SetShadowOffset(0, -0)

local DejaVuSansFont = CreateFont("DejaVuSansFont")
DejaVuSansFont:SetFont([[Interface\AddOns\KkthnxUI\Media\Fonts\DejaVuSans.ttf]], 12)
DejaVuSansFont:SetShadowColor(0, 0, 0, 1)
DejaVuSansFont:SetShadowOffset(1.25, -1.25)

local DejaVuSansFontOutline = CreateFont("DejaVuSansFontOutline")
DejaVuSansFontOutline:SetFont([[Interface\AddOns\KkthnxUI\Media\Fonts\DejaVuSans.ttf]], 12, "OUTLINE")
DejaVuSansFontOutline:SetShadowColor(0, 0, 0, 0)
DejaVuSansFontOutline:SetShadowOffset(0, -0)

local LiberationSansFont = CreateFont("LiberationSansFont")
LiberationSansFont:SetFont([[Interface\AddOns\KkthnxUI\Media\Fonts\LiberationSans-Regular.ttf]], 12)
LiberationSansFont:SetShadowColor(0, 0, 0, 1)
LiberationSansFont:SetShadowOffset(1.25, -1.25)

local LiberationSansFontOutline = CreateFont("LiberationSansFontOutline")
LiberationSansFontOutline:SetFont([[Interface\AddOns\KkthnxUI\Media\Fonts\LiberationSans-Regular.ttf]], 12, "OUTLINE")
LiberationSansFontOutline:SetShadowColor(0, 0, 0, 0)
LiberationSansFontOutline:SetShadowOffset(0, -0)

local BlizzardFont = CreateFont("BlizzardFont")
BlizzardFont:SetFont(_G.STANDARD_TEXT_FONT, 12)
BlizzardFont:SetShadowColor(0, 0, 0, 1)
BlizzardFont:SetShadowOffset(1.25, -1.25)

local BlizzardFontOutline = CreateFont("BlizzardFontOutline")
BlizzardFontOutline:SetFont(_G.STANDARD_TEXT_FONT, 12, "OUTLINE")
BlizzardFontOutline:SetShadowColor(0, 0, 0, 0)
BlizzardFontOutline:SetShadowOffset(0, -0)

local TextureTable = {
	["AltzUI"] = C["Media"].AltzUI,
	["AsphyxiaUI"] = C["Media"].AsphyxiaUI,
	["Blank"] = C["Media"].Blank,
	["DiabolicUI"] = C["Media"].DiabolicUI,
	["Flat"] = C["Media"].FlatTexture,
	["GoldpawUI"] = C["Media"].GoldpawUI,
	["KkthnxUI"] = C["Media"].Texture,
	["SkullFlowerUI"] = C["Media"].SkullFlowerUI,
	["Tukui"] = C["Media"].Tukui,
	["ZorkUI"] = C["Media"].ZorkUI,
}

local FontTable = {
	["Blizzard Outline"] = "BlizzardFontOutline",
	["Blizzard"] = "BlizzardFont",
	["Expressway Outline"] = "ExpresswayFontOutline",
	["Expressway"] = "ExpresswayFont",
	["KkthnxUI Outline"] = "KkthnxUIFontOutline",
	["KkthnxUI"] = "KkthnxUIFont",
	["SansNarrow Outline"] = "SansNarrowFontOutline",
	["SansNarrow"] = "SansNarrowFont",
	["DejaVuSans"] = "DejaVuSansFont",
	["DejaVuSans Outline"] = "DejaVuSansFontOutline",
	["LiberationSans"] = "LiberationSansFont",
	["LiberationSans Outline"] = "LiberationSansFontOutline",
}

function K.GetFont(font)
	if FontTable[font] then
		return FontTable[font]
	else
		return FontTable["KkthnxUI"] -- Return something to prevent errors
	end
end

function K.GetTexture(texture)
	if TextureTable[texture] then
		return TextureTable[texture]
	else
		return TextureTable["KkthnxUI"] -- Return something to prevent errors
	end
end

function KkthnxUIMedia:RegisterTexture(name, path)
	if (not TextureTable[name]) then
		TextureTable[name] = path
	end
end

function KkthnxUIMedia:RegisterFont(name, path)
	if (not FontTable[name]) then
		FontTable[name] = path
	end
end

K["Media"] = KkthnxUIMedia
K["FontTable"] = FontTable
K["TextureTable"] = TextureTable