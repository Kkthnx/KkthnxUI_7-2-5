local K = unpack(select(2, ...))

local _G = _G
local print = _G.print

local CreateFrame = _G.CreateFrame
local GetSpellInfo = _G.GetSpellInfo
local IsPlayerSpell = _G.IsPlayerSpell

local function Defaults(priorityOverride)
	return {["enable"] = true, ["priority"] = priorityOverride or 0, ["stackThreshold"] = 0}
end

local function SpellName(id)
	local name = GetSpellInfo(id)
	if not name then
		print("|cff3c9bedKkthnxUI:|r SpellID is not valid: " .. id .. ". Please check for an updated version, if none exists report to Kkthnx in Discord.")
		return "Impale"
	else
		return name
	end
end

K.DebuffsTracking = {}
K.TicksPenance = SpellName(47540)

K.DebuffsTracking["RaidDebuffs"] = {
	["type"] = "Whitelist",
	["spells"] = {
		-- Legion
		-- Tomb of Sargeras
		-- Goroth
		[233279] = Defaults(), -- Shattering Star
		[230345] = Defaults(), -- Crashing Comet
		[231363] = Defaults(), -- Burning Armor
		[234264] = Defaults(), -- Melted Armor
		[233062] = Defaults(), -- Infernal Burning

		-- Demonic Inquisition
		[233430] = Defaults(), -- Ubearable Torment
		[233983] = Defaults(), -- Echoing Anguish

		-- Harjatan
		[231770] = Defaults(), -- Drenched
		[231998] = Defaults(), -- Jagged Abrasion
		[231729] = Defaults(), -- Aqueous Burst
		[234128] = Defaults(), -- Driven Assault

		-- Sisters of the Moon
		[236603] = Defaults(), -- Rapid Shot
		[236598] = Defaults(5), -- Rapid Shot 1
		[234995] = Defaults(5), -- Rapid Shot 2
		[236519] = Defaults(4), -- Moon Burn
		[236697] = Defaults(), -- Deathly Screech
		[239264] = Defaults(), -- Lunar Flare (Tank)
		[236712] = Defaults(5), -- Lunar Beacon
		[236304] = Defaults(), -- Incorporeal Shot
		[236550] = Defaults(), -- Discorporate (Tank)
		[236330] = Defaults(), -- Astral Vulnerability
		[236541] = Defaults(), -- Twilight Glaive
		[233263] = Defaults(), -- Embrace of the Eclipse

		-- Mistress Sassz'ine
		[230959] = Defaults(), -- Concealing Murk
		[232722] = Defaults(), -- Slicing Tornado
		[232913] = Defaults(), -- Befouling Ink
		[234621] = Defaults(), -- Devouring Maw
		[230201] = Defaults(), -- Burden of Pain (Tank)
		[230139] = Defaults(), -- Hydra Shot
		[232754] = Defaults(), -- Hydra Acid
		[230384] = Defaults(), -- Consuming Hunger
		[230358] = Defaults(), -- Thundering Shock

		-- The Desolate Host
		[236072] = Defaults(), -- Wailing Souls
		[236449] = Defaults(), -- Soulbind
		[236515] = Defaults(), -- Shattering Scream
		[235989] = Defaults(), -- Tormented Cries
		[236241] = Defaults(), -- Soul Rot
		[236361] = Defaults(), -- Spirit Chains
		[235968] = Defaults(), -- Grasping Darkness

		-- Maiden of Vigilance
		[235117] = Defaults(), -- Unstable Soul !needs review
		[240209] = Defaults(), -- Unstable Soul !needs review
		[235534] = Defaults(), -- Creator's Grace
		[235538] = Defaults(), -- Demon's Vigor
		[234891] = Defaults(), -- Wrath of the Creators
		[235569] = Defaults(), -- Hammer of Creation
		[235573] = Defaults(), -- Hammer of Obliteration
		[235213] = Defaults(), -- Light Infusion
		[235240] = Defaults(), -- Fel Infusion

		-- Fallen Avatar
		[239058] = Defaults(), -- Touch of Sargeras
		[239739] = Defaults(), -- Dark Mark
		[234059] = Defaults(), -- Unbound Chaos
		[240213] = Defaults(), -- Chaos Flames
		[236604] = Defaults(), -- Shadowy Blades
		[236494] = Defaults(), -- Desolate (Tank)

		-- Kil'jaeden
		[238999] = Defaults(), -- Darkness of a Thousand Souls
		[239216] = Defaults(), -- Darkness of a Thousand Souls (Dot)
		[239155] = Defaults(), -- Gravity Squeeze
		[234295] = Defaults(), -- Armageddon Rain
		[240908] = Defaults(), -- Armageddon Blast
		[239932] = Defaults(), -- Felclaws (Tank)
		[240911] = Defaults(), -- Armageddon Hail
		[238505] = Defaults(), -- Focused Dreadflame
		[238429] = Defaults(), -- Bursting Dreadflame
		[236710] = Defaults(), -- Shadow Reflection: Erupting
		[241822] = Defaults(), -- Choking Shadow
		[236555] = Defaults(), -- Deceiver's Veil

		-- The Nighthold
		-- Skorpyron
		[204766] = Defaults(), -- Energy Surge
		[214718] = Defaults(), -- Acidic Fragments
		[211801] = Defaults(), -- Volatile Fragments
		[204284] = Defaults(), -- Broken Shard (Protection)
		[204275] = Defaults(), -- Arcanoslash (Tank)
		[211659] = Defaults(), -- Arcane Tether (Tank debuff)
		[204483] = Defaults(), -- Focused Blast (Stun)

		-- Chronomatic Anomaly
		[206607] = Defaults(), -- Chronometric Particles (Tank stack debuff)
		[206609] = Defaults(), -- Time Release (Heal buff/debuff)
		[219966] = Defaults(), -- Time Release (Heal Absorb Red)
		[219965] = Defaults(), -- Time Release (Heal Absorb Yellow)
		[219964] = Defaults(), -- Time Release (Heal Absorb Green)
		[205653] = Defaults(), -- Passage of Time
		[225901] = Defaults(), -- Time Bomb
		[207871] = Defaults(), -- Vortex (Mythic)
		[212099] = Defaults(), -- Temporal Charge

		-- Trilliax
		[206488] = Defaults(), -- Arcane Seepage
		[206641] = Defaults(), -- Arcane Spear (Tank)
		[206798] = Defaults(), -- Toxic Slice
		[214672] = Defaults(), -- Annihilation
		[214573] = Defaults(), -- Stuffed
		[214583] = Defaults(), -- Sterilize
		[208910] = Defaults(), -- Arcing Bonds
		[206838] = Defaults(), -- Succulent Feast

		-- Spellblade Aluriel
		[212492] = Defaults(), -- Annihilate (Tank)
		[212494] = Defaults(), -- Annihilated (Main Tank debuff)
		[212587] = Defaults(), -- Mark of Frost
		[212531] = Defaults(), -- Mark of Frost (marked)
		[212530] = Defaults(), -- Replicate: Mark of Frost
		[212647] = Defaults(), -- Frostbitten
		[212736] = Defaults(), -- Pool of Frost
		[213085] = Defaults(), -- Frozen Tempest
		[213621] = Defaults(), -- Entombed in Ice
		[213148] = Defaults(), -- Searing Brand Chosen
		[213181] = Defaults(), -- Searing Brand Stunned
		[213166] = Defaults(), -- Searing Brand
		[213278] = Defaults(), -- Burning Ground
		[213504] = Defaults(), -- Arcane Fog

		-- Tichondrius
		[206480] = Defaults(), -- Carrion Plague
		[215988] = Defaults(), -- Carrion Nightmare
		[208230] = Defaults(), -- Feast of Blood
		[212794] = Defaults(), -- Brand of Argus
		[216685] = Defaults(), -- Flames of Argus
		[206311] = Defaults(), -- Illusionary Night
		[206466] = Defaults(), -- Essence of Night
		[216024] = Defaults(), -- Volatile Wound
		[216027] = Defaults(), -- Nether Zone
		[216039] = Defaults(), -- Fel Storm
		[216726] = Defaults(), -- Ring of Shadows
		[216040] = Defaults(), -- Burning Soul

		-- Krosus
		[206677] = Defaults(), -- Searing Brand
		[205344] = Defaults(), -- Orb of Destruction

		-- High Botanist Tel'arn
		[218503] = Defaults(), -- Recursive Strikes (Tank)
		[219235] = Defaults(), -- Toxic Spores
		[218809] = Defaults(), -- Call of Night
		[218342] = Defaults(), -- Parasitic Fixate
		[218304] = Defaults(), -- Parasitic Fetter
		[218780] = Defaults(), -- Plasma Explosion

		-- Star Augur Etraeus
		[205984] = Defaults(), -- Gravitaional Pull
		[214167] = Defaults(), -- Gravitaional Pull
		[214335] = Defaults(), -- Gravitaional Pull
		[206936] = Defaults(), -- Icy Ejection
		[206388] = Defaults(), -- Felburst
		[206585] = Defaults(), -- Absolute Zero
		[206398] = Defaults(), -- Felflame
		[206589] = Defaults(), -- Chilled
		[205649] = Defaults(), -- Fel Ejection
		[206965] = Defaults(), -- Voidburst
		[206464] = Defaults(), -- Coronal Ejection
		[207143] = Defaults(), -- Void Ejection
		[206603] = Defaults(), -- Frozen Solid
		[207720] = Defaults(), -- Witness the Void
		[216697] = Defaults(), -- Frigid Pulse

		-- Grand Magistrix Elisande
		[209166] = Defaults(), -- Fast Time
		[211887] = Defaults(), -- Ablated
		[209615] = Defaults(), -- Ablation
		[209244] = Defaults(), -- Delphuric Beam
		[209165] = Defaults(), -- Slow Time
		[209598] = Defaults(), -- Conflexive Burst
		[209433] = Defaults(), -- Spanning Singularity
		[209973] = Defaults(), -- Ablating Explosion
		[209549] = Defaults(), -- Lingering Burn
		[211261] = Defaults(), -- Permaliative Torment
		[208659] = Defaults(), -- Arcanetic Ring

		-- Gul'dan
		[210339] = Defaults(), -- Time Dilation
		[180079] = Defaults(), -- Felfire Munitions
		[206875] = Defaults(), -- Fel Obelisk (Tank)
		[206840] = Defaults(), -- Gaze of Vethriz
		[206896] = Defaults(), -- Torn Soul
		[206221] = Defaults(), -- Empowered Bonds of Fel
		[208802] = Defaults(), -- Soul Corrosion
		[212686] = Defaults(), -- Flames of Sargeras

		-- The Emerald Nightmare
		-- Nythendra
		[204504] = Defaults(), -- Infested
		[205043] = Defaults(), -- Infested mind
		[203096] = Defaults(), -- Rot
		[204463] = Defaults(), -- Volatile Rot
		[203045] = Defaults(), -- Infested Ground
		[203646] = Defaults(), -- Burst of Corruption

		-- Elerethe Renferal
		[210228] = Defaults(), -- Dripping Fangs
		[215307] = Defaults(), -- Web of Pain
		[215300] = Defaults(), -- Web of Pain
		[215460] = Defaults(), -- Necrotic Venom
		[213124] = Defaults(), -- Venomous Pool
		[210850] = Defaults(), -- Twisting Shadows
		[215489] = Defaults(), -- Venomous Pool
		[218519] = Defaults(), -- Wind Burn (Mythic)

		-- Il'gynoth, Heart of the Corruption
		[208929] = Defaults(), -- Spew Corruption
		[210984] = Defaults(), -- Eye of Fate
		[209469] = Defaults(5), -- Touch of Corruption
		[208697] = Defaults(), -- Mind Flay
		[215143] = Defaults(), -- Cursed Blood

		-- Ursoc
		[198108] = Defaults(), -- Unbalanced
		[197943] = Defaults(), -- Overwhelm
		[204859] = Defaults(), -- Rend Flesh
		[205611] = Defaults(), -- Miasma
		[198006] = Defaults(), -- Focused Gaze
		[197980] = Defaults(), -- Nightmarish Cacophony

		-- Dragons of Nightmare
		[203102] = Defaults(), -- Mark of Ysondre
		[203121] = Defaults(), -- Mark of Taerar
		[203125] = Defaults(), -- Mark of Emeriss
		[203124] = Defaults(), -- Mark of Lethon
		[204731] = Defaults(5), -- Wasting Dread
		[203110] = Defaults(5), -- Slumbering Nightmare
		[207681] = Defaults(5), -- Nightmare Bloom
		[205341] = Defaults(5), -- Sleeping Fog
		[203770] = Defaults(5), -- Defiled Vines
		[203787] = Defaults(5), -- Volatile Infection

		-- Cenarius
		[210279] = Defaults(), -- Creeping Nightmares
		[213162] = Defaults(), -- Nightmare Blast
		[210315] = Defaults(), -- Nightmare Brambles
		[212681] = Defaults(), -- Cleansed Ground
		[211507] = Defaults(), -- Nightmare Javelin
		[211471] = Defaults(), -- Scorned Touch
		[211612] = Defaults(), -- Replenishing Roots
		[216516] = Defaults(), -- Ancient Dream

		-- Xavius
		[206005] = Defaults(), -- Dream Simulacrum
		[206651] = Defaults(), -- Darkening Soul
		[209158] = Defaults(), -- Blackening Soul
		[211802] = Defaults(), -- Nightmare Blades
		[206109] = Defaults(), -- Awakening to the Nightmare
		[209034] = Defaults(), -- Bonds of Terror
		[210451] = Defaults(), -- Bonds of Terror
		[208431] = Defaults(), -- Corruption: Descent into Madness
		[207409] = Defaults(), -- Madness
		[211634] = Defaults(), -- The Infinite Dark
		[208385] = Defaults(), -- Tainted Discharge

		-- Trial of Valor
		-- Odyn
		[227959] = Defaults(), -- Storm of Justice
		[227807] = Defaults(), -- Storm of Justice
		[227475] = Defaults(), -- Cleansing Flame
		[192044] = Defaults(), -- Expel Light
		[228030] = Defaults(), -- Expel Light
		[227781] = Defaults(), -- Glowing Fragment
		[228918] = Defaults(), -- Stormforged Spear
		[227490] = Defaults(), -- Branded
		[227491] = Defaults(), -- Branded
		[227498] = Defaults(), -- Branded
		[227499] = Defaults(), -- Branded
		[227500] = Defaults(), -- Branded
		[231297] = Defaults(), -- Runic Brand (Mythic Only)

		-- Guarm
		[228228] = Defaults(), -- Flame Lick
		[228248] = Defaults(), -- Frost Lick
		[228253] = Defaults(), -- Shadow Lick
		[227539] = Defaults(), -- Fiery Phlegm
		[227566] = Defaults(), -- Salty Spittle
		[227570] = Defaults(), -- Dark Discharge

		-- Helya
		[228883] = Defaults(5), -- Unholy Reckoning (Trash)
		[227903] = Defaults(), -- Orb of Corruption
		[228058] = Defaults(), -- Orb of Corrosion
		[229119] = Defaults(), -- Orb of Corrosion
		[228054] = Defaults(), -- Taint of the Sea
		[193367] = Defaults(), -- Fetid Rot
		[227982] = Defaults(), -- Bilewater Redox
		[228519] = Defaults(), -- Anchor Slam
		[202476] = Defaults(), -- Rabid
		[232450] = Defaults(), -- Corrupted Axion
	}
}

-- CC DEBUFFS (TRACKING LIST)
K.DebuffsTracking["CCDebuffs"] = {
	["type"] = "Whitelist",
	["spells"] = {
		--Death Knight
		[47476] = Defaults(2), --Strangulate
		[108194] = Defaults(4), --Asphyxiate UH
		[221562] = Defaults(4), --Asphyxiate Blood
		[207171] = Defaults(4), --Winter is Coming
		[206961] = Defaults(3), --Tremble Before Me
		[207167] = Defaults(4), --Blinding Sleet
		[212540] = Defaults(1), --Flesh Hook (Pet)
		[91807] = Defaults(1), --Shambling Rush (Pet)
		[204085] = Defaults(1), --Deathchill
		[233395] = Defaults(1), --Frozen Center
		[212332] = Defaults(4), --Smash (Pet)
		[212337] = Defaults(4), --Powerful Smash (Pet)
		[91800] = Defaults(4), --Gnaw (Pet)
		[91797] = Defaults(4), --Monstrous Blow (Pet)
		--[?????] = Defaults(), --Reanimation (missing data)
		[210141] = Defaults(3), --Zombie Explosion
		--Demon Hunter
		[207685] = Defaults(4), --Sigil of Misery
		[217832] = Defaults(3), --Imprison
		[221527] = Defaults(5), --Imprison (Banished version)
		[204490] = Defaults(2), --Sigil of Silence
		[179057] = Defaults(3), --Chaos Nova
		[211881] = Defaults(4), --Fel Eruption
		[205630] = Defaults(3), --Illidan's Grasp
		[208618] = Defaults(3), --Illidan's Grasp (Afterward)
		[213491] = Defaults(4), --Demonic Trample (it's this one or the other)
		[208645] = Defaults(4), --Demonic Trample
		[200166] = Defaults(4), --Metamorphosis
		--Druid
		[81261] = Defaults(2), --Solar Beam
		[5211] = Defaults(4), --Mighty Bash
		[163505] = Defaults(4), --Rake
		[203123] = Defaults(4), --Maim
		[202244] = Defaults(4), --Overrun
		[99] = Defaults(4), --Incapacitating Roar
		[33786] = Defaults(5), --Cyclone
		[209753] = Defaults(5), --Cyclone Balance
		[45334] = Defaults(1), --Immobilized
		[102359] = Defaults(1), --Mass Entanglement
		[339] = Defaults(1), --Entangling Roots
		--Hunter
		[202933] = Defaults(2), --Spider Sting (it's this one or the other)
		[233022] = Defaults(2), --Spider Sting
		[224729] = Defaults(4), --Bursting Shot
		[213691] = Defaults(4), --Scatter Shot
		[19386] = Defaults(3), --Wyvern Sting
		[3355] = Defaults(3), --Freezing Trap
		[203337] = Defaults(5), --Freezing Trap (Survival PvPT)
		[209790] = Defaults(3), --Freezing Arrow
		[24394] = Defaults(4), --Intimidation
		[117526] = Defaults(4), --Binding Shot
		[190927] = Defaults(1), --Harpoon
		[201158] = Defaults(1), --Super Sticky Tar
		[162480] = Defaults(1), --Steel Trap
		[212638] = Defaults(1), --Tracker's Net
		[200108] = Defaults(1), --Ranger's Net
		--Mage
		[61721] = Defaults(3), --Rabbit (Poly)
		[61305] = Defaults(3), --Black Cat (Poly)
		[28272] = Defaults(3), --Pig (Poly)
		[28271] = Defaults(3), --Turtle (Poly)
		[126819] = Defaults(3), --Porcupine (Poly)
		[161354] = Defaults(3), --Monkey (Poly)
		[161353] = Defaults(3), --Polar bear (Poly)
		[118] = Defaults(3), --Polymorph
		[82691] = Defaults(3), --Ring of Frost
		[31661] = Defaults(3), --Dragon's Breath
		[122] = Defaults(1), --Frost Nova
		[33395] = Defaults(1), --Freeze
		[157997] = Defaults(1), --Ice Nova
		[228600] = Defaults(1), --Glacial Spike
		[198121] = Defaults(1), --Forstbite
		--Monk
		[119381] = Defaults(4), --Leg Sweep
		[202346] = Defaults(4), --Double Barrel
		[115078] = Defaults(4), --Paralysis
		[198909] = Defaults(3), --Song of Chi-Ji
		[202274] = Defaults(3), --Incendiary Brew
		[233759] = Defaults(2), --Grapple Weapon
		[123407] = Defaults(1), --Spinning Fire Blossom
		[116706] = Defaults(1), --Disable
		[232055] = Defaults(4), --Fists of Fury (it's this one or the other)
		--Paladin
		[853] = Defaults(3), --Hammer of Justice
		[20066] = Defaults(3), --Repentance
		[105421] = Defaults(3), --Blinding Light
		[31935] = Defaults(2), --Avenger's Shield
		[217824] = Defaults(2), --Shield of Virtue
		[205290] = Defaults(3), --Wake of Ashes
		--Priest
		[9484] = Defaults(3), --Shackle Undead
		[200196] = Defaults(4), --Holy Word: Chastise
		[200200] = Defaults(4), --Holy Word: Chastise
		[226943] = Defaults(3), --Mind Bomb
		[605] = Defaults(5), --Mind Control
		[8122] = Defaults(3), --Psychic Scream
		[15487] = Defaults(2), --Silence
		[199683] = Defaults(2), --Last Word
		--Rogue
		[2094] = Defaults(4), --Blind
		[6770] = Defaults(4), --Sap
		[1776] = Defaults(4), --Gouge
		[199743] = Defaults(4), --Parley
		[1330] = Defaults(2), --Garrote - Silence
		[207777] = Defaults(2), --Dismantle
		[199804] = Defaults(4), --Between the Eyes
		[408] = Defaults(4), --Kidney Shot
		[1833] = Defaults(4), --Cheap Shot
		[207736] = Defaults(5), --Shadowy Duel (Smoke effect)
		[212182] = Defaults(5), --Smoke Bomb
		--Shaman
		[51514] = Defaults(3), --Hex
		[211015] = Defaults(3), --Hex (Cockroach)
		[211010] = Defaults(3), --Hex (Snake)
		[211004] = Defaults(3), --Hex (Spider)
		[210873] = Defaults(3), --Hex (Compy)
		[196942] = Defaults(3), --Hex (Voodoo Totem)
		[118905] = Defaults(3), --Static Charge
		[77505] = Defaults(4), --Earthquake (Knocking down)
		[118345] = Defaults(4), --Pulverize (Pet)
		[204399] = Defaults(3), --Earthfury
		[204437] = Defaults(3), --Lightning Lasso
		[157375] = Defaults(4), --Gale Force
		[64695] = Defaults(1), --Earthgrab
		--Warlock
		[710] = Defaults(5), --Banish
		[6789] = Defaults(3), --Mortal Coil
		[118699] = Defaults(3), --Fear
		[5484] = Defaults(3), --Howl of Terror
		[6358] = Defaults(3), --Seduction (Succub)
		[171017] = Defaults(4), --Meteor Strike (Infernal)
		[22703] = Defaults(4), --Infernal Awakening (Infernal CD)
		[30283] = Defaults(3), --Shadowfury
		[89766] = Defaults(4), --Axe Toss
		[233582] = Defaults(1), --Entrenched in Flame
		--Warrior
		[5246] = Defaults(4), --Intimidating Shout
		[7922] = Defaults(4), --Warbringer
		[132169] = Defaults(4), --Storm Bolt
		[132168] = Defaults(4), --Shockwave
		[199085] = Defaults(4), --Warpath
		[105771] = Defaults(1), --Charge
		[199042] = Defaults(1), --Thunderstruck
		--Racial
		[155145] = Defaults(2), --Arcane Torrent
		[20549] = Defaults(4), --War Stomp
		[107079] = Defaults(4), --Quaking Palm
	}
}

-- Raid Buffs (Squared Aura Tracking List)
K.RaidBuffsTracking = {
	PRIEST = {
		{194384, "TOPRIGHT", {1, 0, 0.75}}, -- Atonement
		{41635, "BOTTOMRIGHT", {0.2, 0.7, 0.2}}, -- Prayer of Mending
		{139, "BOTTOMLEFT", {0.4, 0.7, 0.2}}, -- Renew
		{17, "TOPLEFT", {0.81, 0.85, 0.1}, true}, -- Power Word: Shield
		{47788, "LEFT", {221/255, 117/255, 0}, true}, -- Guardian Spirit
		{33206, "LEFT", {227/255, 23/255, 13/255}, true}, -- Pain Suppression
	},

	DRUID = {
		{774, "TOPRIGHT", {0.8, 0.4, 0.8}}, -- Rejuvenation
		{155777, "RIGHT", {0.8, 0.4, 0.8}}, -- Germination
		{8936, "BOTTOMLEFT", {0.2, 0.8, 0.2}}, -- Regrowth
		{33763, "TOPLEFT", {0.4, 0.8, 0.2}}, -- Lifebloom
		{188550, "TOPLEFT", {0.4, 0.8, 0.2}}, -- Lifebloom T18 4pc
		{48438, "BOTTOMRIGHT", {0.8, 0.4, 0}}, -- Wild Growth
		{207386, "TOP", {0.4, 0.2, 0.8}}, -- Spring Blossoms
		{102351, "LEFT", {0.2, 0.8, 0.8}}, -- Cenarion Ward (Initial Buff)
		{102352, "LEFT", {0.2, 0.8, 0.8}}, -- Cenarion Ward (HoT)
		{200389, "BOTTOM", {1, 1, 0.4}}, -- Cultivation
	},

	PALADIN = {
		{53563, "TOPRIGHT", {0.7, 0.3, 0.7}}, -- Beacon of Light
		{156910, "TOPRIGHT", {0.7, 0.3, 0.7}}, -- Beacon of Faith
		--{200025, "TOPRIGHT", {0.7, 0.3, 0.7}}, -- Beacon of Virtue
		--{287280, "RIGHT", {0.7, 0.3, 0.7}}, -- Glimmer of Light
		{1022, "BOTTOMRIGHT", {0.2, 0.2, 1}, true}, -- Hand of Protection
		{1044, "BOTTOMRIGHT", {0.89, 0.45, 0}, true}, -- Hand of Freedom
		{6940, "BOTTOMRIGHT", {0.89, 0.1, 0.1}, true}, -- Hand of Sacrifice
		{223306, "BOTTOMLEFT", {0.7, 0.7, 0.3}, true} -- Bestow Faith
	},

	SHAMAN = {
		{61295, "TOPRIGHT", {0.7, 0.3, 0.7}}, -- Riptide
	},

	MONK = {
		{119611, "TOPLEFT", {0.8, 0.4, 0.8}}, --Renewing Mist
		{116849, "TOPRIGHT", {0.2, 0.8, 0.2}}, -- Life Cocoon
		{124682, "BOTTOMLEFT", {0.4, 0.8, 0.2}}, -- Enveloping Mist
		{124081, "BOTTOMRIGHT", {0.7, 0.4, 0}} -- Zen Sphere
	},

	ROGUE = {
		{57934, "TOPRIGHT", {227 / 255, 23 / 255, 13 / 255}} -- Tricks of the Trade
	},

	WARRIOR = {
		{114030, "TOPLEFT", {0.2, 0.2, 1}}, -- Vigilance
		{3411, "TOPRIGHT", {227 / 255, 23 / 255, 13 / 255}} -- Intervene
	},

	PET = {
		-- Hunter Pets
		{19615, "TOPLEFT", {227 / 255, 23 / 255, 13 / 255}, true}, -- Frenzy
		{136, "TOPRIGHT", {0.2, 0.8, 0.2}, true} -- Mend Pet
	},



	-- PALADIN = {
		-- 	[53563] = ClassBuff(53563, "TOPRIGHT", {0.7, 0.3, 0.7}), -- Beacon of Light
		-- 	[156910] = ClassBuff(156910, "TOPRIGHT", {0.7, 0.3, 0.7}), -- Beacon of Faith
		-- 	[1022] = ClassBuff(1022, "BOTTOMRIGHT", {0.2, 0.2, 1}, true), -- Hand of Protection
		-- 	[1044] = ClassBuff(1044, "BOTTOMRIGHT", {0.89, 0.45, 0}, true), -- Hand of Freedom
		-- 	[6940] = ClassBuff(6940, "BOTTOMRIGHT", {0.89, 0.1, 0.1}, true), -- Hand of Sacrifice
		-- 	[114163] = ClassBuff(114163, 'BOTTOMLEFT', {0.87, 0.7, 0.03}), -- Eternal Flame
	-- },
	-- SHAMAN = {
		-- 	[61295] = ClassBuff(61295, "TOPRIGHT", {0.7, 0.3, 0.7}), -- Riptide
	-- },
	-- MONK = {
		-- 	[119611] = ClassBuff(119611, "TOPLEFT", {0.8, 0.4, 0.8}), --Renewing Mist
		-- 	[116849] = ClassBuff(116849, "TOPRIGHT", {0.2, 0.8, 0.2}), -- Life Cocoon
		-- 	[124682] = ClassBuff(124682, "BOTTOMLEFT", {0.4, 0.8, 0.2}), -- Enveloping Mist
		-- 	[124081] = ClassBuff(124081, "BOTTOMRIGHT", {0.7, 0.4, 0}), -- Zen Sphere
	-- },
	-- ROGUE = {
		-- 	[57934] = ClassBuff(57934, "TOPRIGHT", {227/255, 23/255, 13/255}), -- Tricks of the Trade
	-- },w
	-- WARRIOR = {
		-- 	[114030] = ClassBuff(114030, "TOPLEFT", {0.2, 0.2, 1}), -- Vigilance
		-- 	[3411] = ClassBuff(3411, "TOPRIGHT", {227/255, 23/255, 13/255}), -- Intervene
	-- },
	-- PET = {
		-- 	[19615] = ClassBuff(19615, 'TOPLEFT', {227/255, 23/255, 13/255}, true), -- Frenzy
		-- 	[136] = ClassBuff(136, 'TOPRIGHT', {0.2, 0.8, 0.2}, true) --Mend Pet
	-- },
	-- HUNTER = {}, --Keep even if it's an empty table, so a reference to G.unitframe.buffwatch[E.myclass][SomeValue] doesn't trigger error
	-- DEMONHUNTER = {},
	-- WARLOCK = {},
	-- MAGE = {},
	-- DEATHKNIGHT = {},
}

K.RaidBuffsTrackingPosition = {
	TOPLEFT = {6, 1},
	TOPRIGHT = {-6, 1},
	BOTTOMLEFT = {6, 1},
	BOTTOMRIGHT = {-6, 1},
	LEFT = {6, 1},
	RIGHT = {-6, 1},
	TOP = {0, 0},
	BOTTOM = {0, 0}
}

K.DebuffHighlightColors = {
	[25771] = {
		enable = false,
		style = "FILL",
		color = {r = 0.85, g = 0, b = 0, a = 0.85}
	},
}

-- Filter this. Pointless to see.
K.UnimportantBuffs = {
	[SpellName(113942)] = true, -- Demonic: Gateway
	[SpellName(117870)] = true, -- Touch of The Titans
	[SpellName(123981)] = true, -- Perdition
	[SpellName(124273)] = true, -- Stagger
	[SpellName(124274)] = true, -- Stagger
	[SpellName(124275)] = true, -- Stagger
	[SpellName(126434)] = true, -- Tushui Champion
	[SpellName(126436)] = true, -- Huojin Champion
	[SpellName(131493)] = true, -- B.F.F. Friends forever!
	[SpellName(143625)] = true, -- Brawling Champion
	[SpellName(15007)] = true, -- Ress Sickness
	[SpellName(170616)] = true, -- Pet Deserter
	[SpellName(182957)] = true, -- Treasures of Stormheim
	[SpellName(182958)] = true, -- Treasures of Azsuna
	[SpellName(185719)] = true, -- Treasures of Val"sharah
	[SpellName(186401)] = true, -- Sign of the Skirmisher
	[SpellName(186403)] = true, -- Sign of Battle
	[SpellName(186404)] = true, -- Sign of the Emissary
	[SpellName(186406)] = true, -- Sign of the Critter
	[SpellName(188741)] = true, -- Treasures of Highmountain
	[SpellName(199416)] = true, -- Treasures of Suramar
	[SpellName(225787)] = true, -- Sign of the Warrior
	[SpellName(225788)] = true, -- Sign of the Emissary
	[SpellName(227723)] = true, -- Mana Divining Stone
	[SpellName(231115)] = true, -- Treasures of Broken Shore
	[SpellName(233641)] = true, -- Legionfall Commander
	[SpellName(23445)] = true, -- Evil Twin
	[SpellName(237137)] = true, -- Knowledgeable
	[SpellName(237139)] = true, -- Power Overwhelming
	[SpellName(239645)] = true, -- Fel Treasures
	[SpellName(239647)] = true, -- Epic Hunter
	[SpellName(239648)] = true, -- Forces of the Order
	[SpellName(239966)] = true, -- War Effort
	[SpellName(239967)] = true, -- Seal Your Fate
	[SpellName(239968)] = true, -- Fate Smiles Upon You
	[SpellName(239969)] = true, -- Netherstorm
	[SpellName(240979)] = true, -- Reputable
	[SpellName(240980)] = true, -- Light As a Feather
	[SpellName(240985)] = true, -- Reinforced Reins
	[SpellName(240986)] = true, -- Worthy Champions
	[SpellName(240987)] = true, -- Well Prepared
	[SpellName(240989)] = true, -- Heavily Augmented
	[SpellName(24755)] = true, -- Tricked or Treated
	[SpellName(25163)] = true, -- Oozeling"s Disgusting Aura
	[SpellName(26013)] = true, -- Deserter
	[SpellName(36032)] = true, -- Arcane Charge
	[SpellName(36893)] = true, -- Transporter Malfunction
	[SpellName(36900)] = true, -- Soul Split: Evil!
	[SpellName(36901)] = true, -- Soul Split: Good
	[SpellName(39953)] = true, -- A"dal"s Song of Battle
	[SpellName(41425)] = true, -- Hypothermia
	[SpellName(44212)] = true, -- Jack-o"-Lanterned!
	[SpellName(55711)] = true, -- Weakened Heart
	[SpellName(57723)] = true, -- Exhaustion (heroism debuff)
	[SpellName(57724)] = true, -- Sated (lust debuff)
	[SpellName(57819)] = true, -- Argent Champion
	[SpellName(57820)] = true, -- Ebon Champion
	[SpellName(57821)] = true, -- Champion of the Kirin Tor
	[SpellName(58539)] = true, -- Watcher"s Corpse
	[SpellName(71041)] = true, -- Dungeon Deserter
	[SpellName(72968)] = true, -- Precious"s Ribbon
	[SpellName(80354)] = true, -- Temporal Displacement (timewarp debuff)
	[SpellName(8326)] = true, -- Ghost
	[SpellName(85612)] = true, -- Fiona"s Lucky Charm
	[SpellName(85613)] = true, -- Gidwin"s Weapon Oil
	[SpellName(85614)] = true, -- Tarenar"s Talisman
	[SpellName(85615)] = true, -- Pamela"s Doll
	[SpellName(85616)] = true, -- Vex"tul"s Armbands
	[SpellName(85617)] = true, -- Argus" Journal
	[SpellName(85618)] = true, -- Rimblat"s Stone
	[SpellName(85619)] = true, -- Beezil"s Cog
	[SpellName(8733)] = true, -- Blessing of Blackfathom
	[SpellName(89140)] = true, -- Demonic Rebirth: Cooldown
	[SpellName(93337)] = true, -- Champion of Ramkahen
	[SpellName(93339)] = true, -- Champion of the Earthen Ring
	[SpellName(93341)] = true, -- Champion of the Guardians of Hyjal
	[SpellName(93347)] = true, -- Champion of Therazane
	[SpellName(93368)] = true, -- Champion of the Wildhammer Clan
	[SpellName(93795)] = true, -- Stormwind Champion
	[SpellName(93805)] = true, -- Ironforge Champion
	[SpellName(93806)] = true, -- Darnassus Champion
	[SpellName(93811)] = true, -- Exodar Champion
	[SpellName(93816)] = true, -- Gilneas Champion
	[SpellName(93821)] = true, -- Gnomeregan Champion
	[SpellName(93825)] = true, -- Orgrimmar Champion
	[SpellName(93827)] = true, -- Darkspear Champion
	[SpellName(93828)] = true, -- Silvermoon Champion
	[SpellName(93830)] = true, -- Bilgewater Champion
	[SpellName(94158)] = true, -- Champion of the Dragonmaw Clan
	[SpellName(94462)] = true, -- Undercity Champion
	[SpellName(94463)] = true, -- Thunder Bluff Champion
	[SpellName(95809)] = true, -- Insanity debuff (hunter pet heroism: ancient hysteria)
	[SpellName(97340)] = true, -- Guild Champion
	[SpellName(97341)] = true, -- Guild Champion
	[SpellName(97821)] = true -- Void-Touched
}

K.ChannelingTicks = {
	[SpellName(740)] = 4, -- 宁静
	[SpellName(755)] = 3, -- 生命通道
	[SpellName(5143)] = 5, -- 奥术飞弹
	[SpellName(12051)] = 3, -- 唤醒
	[SpellName(15407)] = 4, -- 精神鞭笞
	[SpellName(47540)] = 3, -- 苦修
	[SpellName(64843)] = 4, -- 神圣赞美诗
	[SpellName(198590)] = 5, -- 吸取灵魂
	[SpellName(205021)] = 5, -- 冰霜射线
	[SpellName(205065)] = 6, -- 虚空洪流
	[SpellName(234153)] = 5, -- 吸取生命
	-- [SpellName(291944)] = 6, -- 再生
}

local updateTicks = CreateFrame("Frame")
updateTicks:RegisterEvent("PLAYER_LOGIN")
updateTicks:RegisterEvent("ACTIVE_TALENT_GROUP_CHANGED")
updateTicks:SetScript("OnEvent", function()
	if K.Class ~= "PRIEST" then
		return
	end

	local numTicks = 3
	if IsPlayerSpell(193134) then -- Enhanced Mind Flay
		numTicks = 4
	end

	K.ChannelingTicks[K.TicksPenance] = numTicks
end)