## Interface: 70200
## Name: KkthnxUI Config
## Title: |cff4488ffKkthnxUI Config|r
## Notes: Options for KkthnxUI
## Author: Kkthnx
## Version: 9.06
## DefaultState: Enabled
## SavedVariables: KkthnxUIConfigShared, KkthnxUIConfigPerAccount
## SavedVariablesPerCharacter: KkthnxUIConfigNotShared
## RequiredDeps: KkthnxUI

# -- Load Core File
KkthnxUI_Config.lua

# -- Load Locales
Locales\enUS.lua